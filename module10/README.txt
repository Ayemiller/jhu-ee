Assignment 10 - Adam Miller - amill189


INTRO

This assignment involves adding JMS messaging to the SRS registration functionality. When a
user registers for a course, a MapMessage is produced to the topic "RegCourseTopic". Also,
this project adds two consumers of the RegCourseTopic: a Message Driven Bean (MDB)
class that is included in the main SrsAssignment10 war as well as a standalone project that
consumes messages to the topic, called SrsAssignment10StandaloneConsumer.

To support this functionality in Wildfly, a user was added "mquser" using the following command:
add-user.bat -a -u mquser -p mqpassword -g guest

Also, I used the standalone-full.xml configuration of WildFly 14.0.1.Final

Additionally, in order to re-use code, the SrsAssignment10StandaloneConsumer depends on the
SrsAssignment10 first being packaged and installed in the local repository (~/.m2/repository).
Commands to accomplish this are provided below in the BUILD/RUN/DEPLOY INSTRUCTIONS section.
The code that was specifically reused was a class, RegCourseMessageHelper, that has a static
toString method to help print the MapMessage for this assignment. Nearly all of the new code
in the SRS is in RegistrationHandler::produceRegCourseMessage. The Message Driven Bean in the
SRS is located in RegCourseTopicMessageBean.

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The web.xml configuration file specifies a parameter, "CourseCapacity", which is retrieved from 
the namespace java:comp/env/CourseCapacity. It is initially set to 10 and if the config file
is absent, a sane default of 20 is used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/H2_784_JNDI.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI

To login to the page, use adamtest as both the username and password. 
The included database contains the following sample data:

COURSES
    COURSE_ID  	COURSE_TITLE  
    605.784	    Enterprise Computing with Java
    605.785	    Web Services: Frameworks, Processes, Applications
    605.786	    Enterprise System Design and Implementation
    605.787	    Rich Internet Applications with Ajax
    605.788	    Big Data Processing Using Hadoop

STUDENT
    USER_ID  	PASSWORD  	FIRST_NAME  LAST_NAME  	SSN  	    EMAIL  	         ADDRESS  
    adamtest    adamtest    Adam        Test        123-12-1234 amill189@jh.edu  blah st
    adamtest2   adamtest2   adam2       test2       123-12-1234 adamtest2@jh.edu 123 fake st apt 2

REGISTRAR
    COURSE_ID  	USER_ID  
    605.784 adamtest
    605.784 adamtest2
    605.785 adamtest
    605.785 adamtest2
    605.786 adamtest
    605.787 adamtest
    605.788 adamtest

    
CONTENTS OF SUBMISSION

SrsAssignment10 - Java web application generated and built with Maven

SrsAssignment10/pom.xml - Maven POM file
SrsAssignment10/src/main/java/jhu/ep/amiller/* - Java source for assignment 10

SrsAssignment10/src/main/webapp/* - JSP and XHTML
SrsAssignment10/src/main/webapp/META-INF/persistence.xml - defines the JPA/Hibernate properties
SrsAssignment10/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment10/src/main/webapp/WEB-INF/web.xml

SrsAssignment10/target/SrsAssignment10.war - packaged WAR file
SrsAssignment10/target/SrsAssignment10-classes.jar - packaged classes for assignment (required for consumer)

SrsAssignment10StandaloneConsumer - Java application to consume messages from RegCourseTopic
SrsAssignment10StandaloneConsumer/pom.xml - Maven POM file
SrsAssignment10StandaloneConsumer/src/main/java/jhu/ep/amiller/StandaloneRegCourseTopicConsumer.java - Java source for consumer
SrsAssignment10StandaloneConsumer/target/SrsAssignment10StandaloneConsumer.jar - packaged JAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)


BUILD/RUN/DEPLOY INSTRUCTIONS

    For SrsAssignment10:
    Command to build/install into local repo with Maven: mvn clean package install initialize
    Command to deploy to WildFly server: mvn wildfly:deploy

    For SrsAssignment10StandaloneConsumer:
    Command to build/run: mvn compile exec:java -Dexec.mainClass="jhu.ep.amiller.StandaloneRegCourseTopicConsumer"

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment10/ or http://127.0.0.1:8080/SrsAssignment10/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64), PRIMARY KEY (User_ID));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64));
CREATE TABLE REGISTRAR (Course_ID varchar(16), User_ID varchar(64), PRIMARY KEY(Course_ID, User_ID), FOREIGN KEY (Course_ID) references COURSES(Course_ID), FOREIGN KEY (User_ID) references STUDENT(User_ID));

INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.784', 'Enterprise Computing with Java');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.785', 'Web Services: Frameworks, Processes, Applications');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.786', 'Enterprise System Design and Implementation');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.787', 'Rich Internet Applications with Ajax');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.788', 'Big Data Processing Using Hadoop');
INSERT INTO REGISTRAR (Course_ID, User_ID) VALUES ('605.784', 'adamtest'), ('605.785', 'adamtest'), ('605.786', 'adamtest'), ('605.787', 'adamtest'), ('605.788', 'adamtest');


To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/H2_784_JNDI --name=H2_784_DS --connection-url=jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1 --driver-name=h2

To add the JMS topic, I used the following:
jms-topic add --topic-address=RegCourseTopic --entries=RegCourseTopic,java:jboss/exported/RegCourseTopic