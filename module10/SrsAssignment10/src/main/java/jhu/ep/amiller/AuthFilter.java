package jhu.ep.amiller;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns = {"/*"})
/**
 * Web Filter class that prevents a user from accessing unauthorized pages before first logging in
 * to the SRS.
 *
 * @author Adam
 * @date 2021-03-08
 */
public class AuthFilter implements Filter {
  private static final String BEAN_NAME = "regBean";
  private static final String CONTROLLER_NAME = "RegistrationController";
  private static final String LOGIN_PAGE = "index.jsp";
  private static final String ROOT_NAME = "SrsAssignment10/";
  private ServletContext context;

  @Override
  /**
   * Filter logic to prevent the user from accessing unauthorized pages before first sucessfully
   * logging in to SRS.
   */
  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain) throws IOException, ServletException {

    final HttpServletRequest req = (HttpServletRequest) request;
    final HttpServletResponse res = (HttpServletResponse) response;
    final String uri = req.getRequestURI();
    final HttpSession session = req.getSession(false);
    String user = "";

    // retrieve the logged in user id (if logged in)
    if (session != null) {
      user = (String) session.getAttribute("userId");
    }

    // Permit the user's navigation if logged in, otherwise send user back to login page prompting
    // them to log in.
    if (uri.endsWith(ROOT_NAME) || uri.endsWith(".css") || uri.endsWith(LOGIN_PAGE)
        || uri.endsWith(CONTROLLER_NAME) && session != null || user != null && !user.isEmpty()) {
      chain.doFilter(request, response);
    } else {
      setErrorMessage(req);
      this.context.log("Unauthorized access of " + uri);
      res.sendRedirect(LOGIN_PAGE);
    }

  }

  @Override
  public void init(final FilterConfig fConfig) throws ServletException {
    this.context = fConfig.getServletContext();
  }

  /**
   * Sets error message to be presented to the user at the login page.
   *
   * @param request - the request
   */
  private void setErrorMessage(final HttpServletRequest request) {
    final RegistrationSupportBean regBean =
        (RegistrationSupportBean) request.getSession().getAttribute(BEAN_NAME);
    if (regBean != null) {
      regBean.setMessage("You must login before accessing any other pages.");
    }
  }

}

