package jhu.ep.amiller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Class used to execute SQL queries that are used in multiple classes.
 *
 * @author Adam
 * @date 2021-03-08
 */
class CommonQueries {


  /**
   * Default constructor.
   */
  CommonQueries() {}

  /**
   * Helper function to return the number of students registered to a given courseId.
   *
   * @param con
   * @param courseId
   * @return
   */
  public int getNumRegistered(final String courseId, final Integer defaultCourseCapcity) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        final String sql = "SELECT COUNT(*) FROM REGISTRAR WHERE COURSE_ID=?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
          statement.setObject(1, courseId);
          try (ResultSet rs = statement.executeQuery()) {
            if (rs.next()) {
              return rs.getInt(1);
            }
          } catch (final SQLException e1) {
            e1.printStackTrace();
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
        // in the event of error, return true to indicate the course is full
        return defaultCourseCapcity;
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return 0;
  }

  public boolean isStudentRegisteredForCourse(final String courseId, final String userId) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        final String sql =
            "SELECT Course_ID, User_ID FROM REGISTRAR WHERE Course_ID=? and User_Id=?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
          statement.setObject(1, courseId);
          statement.setObject(2, userId);
          try (ResultSet rs = statement.executeQuery()) {
            if (rs.next()) {
              return true;
            }
          } catch (final SQLException e1) {
            e1.printStackTrace();
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
        // in the event of error, return false to indicate the student is not registered to the
        // course
        return false;
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Helper function to return the connection.
   *
   * @return the connection if successful, null otherwise
   * @throws SQLException
   */
  private Connection getConnection() throws SQLException {

    try {
      final DataSource ds;
      final Context jndiDbPathContext = new InitialContext();

      // load jndi DataSource/URL specified in WEB-INF/jboss-web.xml
      ds = (DataSource) jndiDbPathContext.lookup("java:comp/env/jndi/srsDb");
      return ds.getConnection();
    } catch (final NamingException e) {
      e.printStackTrace();
    }
    return null;
  }

}
