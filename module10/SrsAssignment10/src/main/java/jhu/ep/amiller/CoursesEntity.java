package jhu.ep.amiller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "COURSES")
/**
 * JPA Entity class that represents the Courses table in our database.
 *
 * @author Adam
 * @date 2021-03-15
 *
 */
public class CoursesEntity implements Serializable {
  private static final long serialVersionUID = 9076840937713751022L;

  @Id
  @Column(name = "Course_ID")
  private String courseId;

  @Column(name = "Course_Title")
  private String courseTitle;

  @OneToMany
  @JoinColumn(name = "Course_ID", referencedColumnName = "Course_ID")
  private List<RegistrarEntity> registrar;

  public CoursesEntity() {
    super();
  }

  public String getCourseId() {
    return this.courseId;
  }

  public String getCourseTitle() {
    return this.courseTitle;
  }

  public List<RegistrarEntity> getRegistrar() {
    return this.registrar;
  }

  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setCourseTitle(final String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public void setRegistrar(final List<RegistrarEntity> registrar) {
    this.registrar = registrar;
  }

  @Override
  public String toString() {
    return this.courseId + " - " + this.courseTitle;
  }

}
