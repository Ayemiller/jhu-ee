package jhu.ep.amiller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@SessionScoped
@Named
/**
 * Bean class to retrieve the course information from the database. Contains setters and getters for
 * the available courses and the number of students registered to the class.
 *
 * @author Adam
 * @date 2021-03-08
 */
public class CoursesSupportBean implements Serializable {
  private static final long serialVersionUID = 4846043115757318416L;
  private String courseTitle;
  private final transient Map<String, Object> courseTitleValue;
  private Integer numberRegistered;

  /**
   * Default constructor. Initializes the courses map with the values from the database.
   */
  public CoursesSupportBean() {
    this.courseTitleValue = retrieveCourses();
  }

  /**
   * Retrieve a map of the courses from the database.
   *
   * @param con - the connection
   * @return Map of the courses from the database if successful, null otherwise.
   */
  public Map<String, Object> getCoursesFromDb(final Connection con) {
    final String sql = "SELECT Course_ID, Course_Title FROM COURSES ORDER BY Course_ID";
    try (PreparedStatement statement = con.prepareStatement(sql)) {
      try (ResultSet rs = statement.executeQuery()) {
        final Map<String, Object> courses = new LinkedHashMap<>();
        while (rs.next()) {
          final String entry = rs.getString(1) + " " + rs.getString(2);
          courses.put(entry, entry);
        }
        return courses;
      } catch (final SQLException e1) {
        e1.printStackTrace();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;

  }

  public String getCourseTitle() {
    return this.courseTitle;
  }

  public Map<String, Object> getCourseTitleValue() {
    return this.courseTitleValue;
  }

  public Integer getNumberRegistered() {
    return this.numberRegistered;
  }

  public void setCourseTitle(final String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public void setNumberRegistered(final Integer numberRegistered) {
    this.numberRegistered = numberRegistered;
  }

  /**
   * Helper function to retrieve the courses form the database.
   *
   * @return map containing the courses from the database
   */
  private Map<String, Object> retrieveCourses() {

    try {
      // load jndi DataSource/URL specified in WEB-INF/jboss-web.xml
      Context jndiDbPathContext = new InitialContext();
      final DataSource ds = (DataSource) jndiDbPathContext.lookup("java:comp/env/jndi/srsDb");

      try (Connection con = ds.getConnection()) {
        jndiDbPathContext = new InitialContext();
        if (con != null) {
          return getCoursesFromDb(con);
        }
      } catch (final SQLException e) {
        e.printStackTrace();
      }
    } catch (final NamingException e1) {
      e1.printStackTrace();
    }
    return null;
  }

}
