package jhu.ep.amiller;

import javax.jms.JMSException;
import javax.jms.MapMessage;

public class RegCourseMessageHelper {

  public static String toString(final MapMessage regCourseMapMessage) {
    final StringBuilder builder = new StringBuilder();
    try {
      builder.append("User ID: ").append(regCourseMapMessage.getString("User_ID"))
          .append(" | Course ID: ").append(regCourseMapMessage.getString("Course_ID"))
          .append(" | Course Name: ").append(regCourseMapMessage.getString("Course_Name"))
          .append(" | Date of Registration: ")
          .append(regCourseMapMessage.getString("Date_of_Registration"));
    } catch (final JMSException e) {
      e.printStackTrace();
    }
    return builder.toString();
  }

  private RegCourseMessageHelper() {
    super();
  }

}
