package jhu.ep.amiller;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * Message-Driven Bean implementation class for: RegCourseTopicMessageBean
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "RegCourseTopic"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")},
    mappedName = "RegCourseTopic")
public class RegCourseTopicMessageBean implements MessageListener {

  /**
   * Default constructor.
   */
  public RegCourseTopicMessageBean() {
    super();
  }

  /**
   * @see MessageListener#onMessage(Message)
   */
  @Override
  public void onMessage(final Message message) {
    final MapMessage mapMessage = (MapMessage) message;
    System.out.println(RegCourseMessageHelper.toString(mapMessage));
  }

}
