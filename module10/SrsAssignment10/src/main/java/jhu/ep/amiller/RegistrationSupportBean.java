package jhu.ep.amiller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Java Bean class to serve as the model for our SRS application. It contains getters and setters
 * for all of the registration related fields that will be entered by the user in the JSP view as
 * well as in the JSF.
 *
 * @author Adam Miller
 * @date 2021-03-08
 */
@SessionScoped
@Named
public class RegistrationSupportBean implements Serializable {
  private static final long serialVersionUID = -8986572310727816616L;
  private String address;
  private String city;
  private String courseId;
  private String email;
  private String firstName;
  private String lastName;
  private int loginAttempts = 0;
  /* This field is used to pass error messages from the controller to the view */
  private String message = "";
  private String password;
  private String registrationResult;
  private Map<String, Long> registrationStatusReport;
  private String ssn1;
  private String ssn2;
  private String ssn3;
  private String state;
  private List<String> studentRegistrationStatusReport;
  private String userId;
  private String zip;

  /**
   * Default constructor
   */
  public RegistrationSupportBean() {
    super();
  }

  public String getAddress() {
    return this.address;
  }

  public String getCity() {
    return this.city;
  }

  public String getCourseId() {
    return this.courseId;
  }

  public String getEmail() {
    return this.email;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public int getLoginAttempts() {
    return this.loginAttempts;
  }

  public String getMessage() {
    return this.message;
  }

  public String getPassword() {
    return this.password;
  }

  public String getRegistrationResult() {
    return this.registrationResult;
  }

  public Map<String, Long> getRegistrationStatusReport() {
    return this.registrationStatusReport;
  }

  public String getSsn1() {
    return this.ssn1;
  }

  public String getSsn2() {
    return this.ssn2;
  }

  public String getSsn3() {
    return this.ssn3;
  }

  public String getState() {
    return this.state;
  }

  public List<String> getStudentRegistrationStatusReport() {
    return this.studentRegistrationStatusReport;
  }

  public String getUserId() {
    return this.userId;
  }

  public String getZip() {
    return this.zip;
  }

  public void setAddress(final String address) {
    this.address = address;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public void setLoginAttempts(final int loginAttempts) {
    this.loginAttempts = loginAttempts;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public void setRegistrationResult(final String registrationResult) {
    this.registrationResult = registrationResult;
  }

  public void setRegistrationStatusReport(final Map<String, Long> report) {
    this.registrationStatusReport = report;
  }

  public void setSsn1(final String ssn1) {
    this.ssn1 = ssn1;
  }

  public void setSsn2(final String ssn2) {
    this.ssn2 = ssn2;
  }

  public void setSsn3(final String ssn3) {
    this.ssn3 = ssn3;
  }

  public void setState(final String state) {
    this.state = state;
  }

  public void setStudentRegistrationStatusReport(final List<String> report) {
    this.studentRegistrationStatusReport = report;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

  public void setZip(final String zip) {
    this.zip = zip;
  }

}
