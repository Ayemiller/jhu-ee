package jhu.ep.amiller;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT")
/**
 * JPA Entity class that represents the Students table in our database.
 *
 * @author Adam
 * @date 2021-03-31
 *
 */
public class StudentEntity implements Serializable {
  private static final long serialVersionUID = -146758375306843048L;

  @Column(name = "Last_Name")
  private String lastName;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinColumn(name = "User_ID", referencedColumnName = "User_ID")
  private List<RegistrarEntity> registrar;

  @Id
  @Column(name = "User_ID")
  private String userId;

  public StudentEntity() {
    super();
  }

  public String getLastName() {
    return this.lastName;
  }

  public List<RegistrarEntity> getRegistrar() {
    return this.registrar;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public void setRegistrar(final List<RegistrarEntity> registrar) {
    this.registrar = registrar;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

  @Override
  public String toString() {
    return this.userId + ": " + " Last Name: " + this.lastName;
  }

}
