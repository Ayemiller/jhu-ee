package jhu.ep.amiller;

import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * first-java-app
 *
 */
public class StandaloneRegCourseTopicConsumer {
  // Set up all the default values

  private static final String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
  private static final String DEFAULT_DESTINATION = "queue/test";
  private static final String DEFAULT_PASSWORD = "mqpassword";
  private static final String DEFAULT_USERNAME = "mquser";
  private static final String INITIAL_CONTEXT_FACTORY =
      "org.wildfly.naming.client.WildFlyInitialContextFactory";
  private static final String PROVIDER_URL = "http-remoting://127.0.0.1:8080";

  public static void main(final String[] args) {

    Context namingContext = null;
    ConnectionFactory connectionFactory = null;
    Session session = null;

    try {
      final String userName = System.getProperty("username", DEFAULT_USERNAME);
      final String password = System.getProperty("password", DEFAULT_PASSWORD);

      // Set up the namingContext for the JNDI lookup
      final Properties env = new Properties();
      env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
      env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, PROVIDER_URL));
      env.put(Context.SECURITY_PRINCIPAL, userName);
      env.put(Context.SECURITY_CREDENTIALS, password);
      namingContext = new InitialContext(env);

      // Perform the JNDI lookups
      final String connectionFactoryString =
          System.getProperty("connection.factory", DEFAULT_CONNECTION_FACTORY);
      connectionFactory = (ConnectionFactory) namingContext.lookup(connectionFactoryString);

    } catch (final NamingException e) {
      e.printStackTrace();
    }

    try (final Connection conn = connectionFactory.createConnection("mquser", "mqpassword")) {
      session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
      final Topic topic = session.createTopic("RegCourseTopic");
      final MessageConsumer consumer = session.createConsumer(topic);
      conn.start();

      while (true) {
        final MapMessage mapMessage = (MapMessage) consumer.receive();
        System.out.println(RegCourseMessageHelper.toString(mapMessage));
      }

    } catch (final JMSException e) {
      e.printStackTrace();

    } finally {
      System.out.println("END!!!");

    }

  }
}
