<%@ page import="javax.naming.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.jms.*" %>

<html>
<head>
    <title>JMS Test</title>

    <META HTTP-EQUIV="Refresh" CONTENT="2" />

</head>
<body>
    <H1>This is a JMS test.</H1>
<%! private int accessCount = 0; %>
<H2>This page get refreshed every 2 sec</h2>
    
<H2>Current count of calls on this page is : <%= ++accessCount %></H2>
<%
            String INITIAL_CONTEXT_FACTORY = "org.wildfly.naming.client.WildFlyInitialContextFactory";
            String PROVIDER_URL = "http-remoting://127.0.0.1:8080";
            String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
            String DEFAULT_DESTINATION = "jms/queue/test";
            String DEFAULT_MESSAGE = "Hello, 605.784 class!";

            Context namingContext = null;

            // Set up the namingContext for the JNDI lookup
            
            Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, PROVIDER_URL));

            namingContext = new InitialContext(env);

            // Perform the JNDI lookups
            String connectionFactoryString = System.getProperty("connection.factory", DEFAULT_CONNECTION_FACTORY);

            ConnectionFactory connectionFactory = (ConnectionFactory) namingContext.lookup(connectionFactoryString);

            String destinationString = System.getProperty("destination", DEFAULT_DESTINATION);

            Destination destination = (Destination) namingContext.lookup(destinationString);

            String content = System.getProperty("message.content", DEFAULT_MESSAGE);

            try (JMSContext context = connectionFactory.createContext("mquser", "mqpassword")) {

                context.createProducer().send(destination, content + " this is a message # " + Integer.toString(accessCount));

            } catch (Exception e) {
                    e.printStackTrace();
              } finally {
                         if (namingContext != null) {
                             try {
                                  namingContext.close();
                             } catch (NamingException e) {
                                    e.printStackTrace();
                               }
                         }
                }

%>

</body>
</html>
