Assignment 11 - Adam Miller - amill189


INTRO

In this assignment, we demonstrate the use of a CMT bean to insert entries into the 
REGISTRAR table. To do this, I added the CMT to SRS and the CMT is used when registering for
classes. The CMT automatically switches back and forth between "short" running and "long" 
running calls to demonstrate the "rollback" functionality inherent to CMT. The "long" running 
method has a 4-second "sleep" before the call to persist. Since I have my Wildfly server's 
default timeout set to 3 seconds, the "long" running call should always fail, and an appropriate 
error message is displayed. The "short" running call should always succeed.

The CMT bean can be found in RegistrarCourseBean.java. The calls to it are made from 
RegistrationHandler::registerForCourse.

To specifically demo the CMT functionality-
1) first log in as adamtest:adamtest
2) click Course Registration
3) select "Withdraw From the Course" and click "Submit"
4) click "Submit" again, to withdraw from 705.784
5) click "Back to Main Page"
6) select "Register To the Course" and click "Submit"
7) click "Submit" again. This will register to 705.684 using the "short" CMT method
8) click "Back to Main Page"
9) select "Withdraw From the Course" and click "Submit"
10) click "Submit" again, to withdraw from 705.784
11) click "Back to Main Page"
12) select "Register To the Course" and click "Submit"
13) click "Submit" again. This will attempt to register to 705.684 using the "long" CMT method 
    (it will fail, due to timeout)
14) click "Back to Main Page"
15) select "Register To the Course" and click "Submit"
16) click "Submit" again. This will register to 705.684 using the "short" CMT method

Also, I used the standalone-full.xml configuration of WildFly 14.0.1.Final

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The web.xml configuration file specifies a parameter, "CourseCapacity", which is retrieved from 
the namespace java:comp/env/CourseCapacity. It is initially set to 10 and if the config file
is absent, a sane default of 20 is used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/H2_784_JNDI.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI

To login to the page, use adamtest as both the username and password. 
The included database contains the following sample data:

COURSES
    COURSE_ID  	COURSE_TITLE  
    605.784	    Enterprise Computing with Java
    605.785	    Web Services: Frameworks, Processes, Applications
    605.786	    Enterprise System Design and Implementation
    605.787	    Rich Internet Applications with Ajax
    605.788	    Big Data Processing Using Hadoop

STUDENT
    USER_ID  	PASSWORD  	FIRST_NAME  LAST_NAME  	SSN  	    EMAIL  	         ADDRESS  
    adamtest    adamtest    Adam        Test        123-12-1234 amill189@jh.edu  blah st
    adamtest2   adamtest2   adam2       test2       123-12-1234 adamtest2@jh.edu 123 fake st apt 2

REGISTRAR
    COURSE_ID  	USER_ID  
    605.784 adamtest
    605.784 adamtest2
    605.785 adamtest
    605.785 adamtest2
    605.786 adamtest
    605.787 adamtest
    605.788 adamtest

    
CONTENTS OF SUBMISSION

SrsAssignment11 - Java web application generated and built with Maven

SrsAssignment11/pom.xml - Maven POM file
SrsAssignment11/src/main/java/jhu/ep/amiller/* - Java source for assignment 11

SrsAssignment11/src/main/resources/META-INF/persistence.xml - JTA/Hibernate properties for CMT

SrsAssignment11/src/main/webapp/* - JSP and XHTML
SrsAssignment11/src/main/webapp/META-INF/persistence.xml - defines the JPA/Hibernate properties
SrsAssignment11/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment11/src/main/webapp/WEB-INF/web.xml

SrsAssignment11/target/SrsAssignment11.war - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)


BUILD/RUN/DEPLOY INSTRUCTIONS

    For SrsAssignment11:
    Command to deploy to WildFly server: mvn wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment11/ or http://127.0.0.1:8080/SrsAssignment11/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64), PRIMARY KEY (User_ID));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64), PRIMARY KEY(Course_ID));
CREATE TABLE REGISTRAR (Course_ID varchar(16), User_ID varchar(64), PRIMARY KEY(Course_ID, User_ID), FOREIGN KEY (Course_ID) references COURSES(Course_ID), FOREIGN KEY (User_ID) references STUDENT(User_ID));

INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.784', 'Enterprise Computing with Java');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.785', 'Web Services: Frameworks, Processes, Applications');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.786', 'Enterprise System Design and Implementation');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.787', 'Rich Internet Applications with Ajax');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.788', 'Big Data Processing Using Hadoop');
INSERT INTO REGISTRAR (Course_ID, User_ID) VALUES ('605.784', 'adamtest'), ('605.785', 'adamtest'), ('605.786', 'adamtest'), ('605.787', 'adamtest'), ('605.788', 'adamtest');


To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/H2_784_JNDI --name=H2_784_DS --connection-url=jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1;AUTO_SERVER=TRUE --driver-name=h2
