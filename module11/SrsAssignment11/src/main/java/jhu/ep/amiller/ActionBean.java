package jhu.ep.amiller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
/**
 * Bean that fires WITHDRAW and REGISTER events based on UI selection. Check server log output for
 * event handling output.
 *
 * @author Adam
 * @date 2021-03-08
 */
public class ActionBean implements Serializable {

  private static final Logger logger = Logger.getLogger(ActionBean.class.getCanonicalName());
  private static final String REGISTER = "Register to ";
  private static final long serialVersionUID = -7936876068393398242L;
  private static final String WITHDRAW = "Withdraw from ";

  private String actionOption = WITHDRAW;
  private Date datetime;
  private String nextAction = null;
  @Inject
  @Register
  Event<ActionEvent> registerEvent;
  @Inject
  @Withdraw
  Event<ActionEvent> withdrawEvent;

  /**
   * Fires a action event.
   *
   * @return the response page location
   */
  public String courseAction(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {

    setDatetime(Calendar.getInstance().getTime());
    switch (this.actionOption) {
      case WITHDRAW:
        final ActionEvent withdrawPayload = new ActionEvent();
        withdrawPayload.setActionType("Withdraw");
        withdrawPayload.setDatetime(this.datetime);
        withdrawPayload.setRegistrationBean(regBean);
        withdrawPayload.setCoursesBean(coursesBean);
        this.withdrawEvent.fire(withdrawPayload);
        this.nextAction = "reg_result";
        break;
      case REGISTER:
        final ActionEvent registerPayload = new ActionEvent();
        registerPayload.setActionType("Register");
        registerPayload.setDatetime(this.datetime);
        registerPayload.setRegistrationBean(regBean);
        registerPayload.setCoursesBean(coursesBean);
        this.registerEvent.fire(registerPayload);
        this.nextAction = "reg_result";
        break;
      default:
        logger.severe("Invalid action option!");
    }
    return this.nextAction;

  }

  public String getActionOption() {
    return this.actionOption;
  }

  public Date getDatetime() {
    return this.datetime;
  }

  /**
   * Resets the values in the index page.
   */
  @Logged
  public void reset() {
    setActionOption(WITHDRAW);
  }

  public void setActionOption(final String actionType) {
    this.actionOption = actionType;
  }

  public void setDatetime(final Date datetime) {
    this.datetime = datetime;
  }

}
