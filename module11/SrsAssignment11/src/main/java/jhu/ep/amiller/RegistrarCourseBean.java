package jhu.ep.amiller;

import java.util.Arrays;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
@ApplicationScoped
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class RegistrarCourseBean {

  @PersistenceContext(unitName = "PU")
  private EntityManager em;


  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public String insertLong(final String courseId, final String userId) throws Exception {

    System.out.println("............ in SingleTonBean insertLong method !!!!!!!!!!!!!!!!!!!");

    final RegistrarEntryEntity regEntity = new RegistrarEntryEntity();
    regEntity.setCourseId(courseId);
    regEntity.setUserId(userId);

    System.out.println("Property object is instantiated !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    Thread.sleep(4000); // 4 seconds
    this.em.persist(regEntity);
    this.em.flush();

    System.out.println("Just did PERSIST and FLUSH !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    return "return from insertShort method";
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public String insertShort(final String courseId, final String userId) throws Exception {

    System.out.println("............ in SingleTonBean insertShort method !!!!!!!!!!!!!!!!!!!");

    final RegistrarEntryEntity regEntity = new RegistrarEntryEntity();
    regEntity.setCourseId(courseId);
    regEntity.setUserId(userId);

    System.out.println("Property object is instantiated !!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    this.em.persist(regEntity);
    this.em.flush();

    System.out.println("Just did PERSIST and FLUSH !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    return "return from insertShort method";
  }

  public int numberOfRecords() {
    System.out.println("----\nnative query ...... numberOfRecords");
    final Query nativeQuery = this.em.createNativeQuery("select count(*) from Property");

    return nativeQuery.getSingleResult() != null
        ? Integer.parseInt(nativeQuery.getSingleResult().toString())
        : 0;
  } // end of numberOfRecords

  public void queryNative(final EntityManager entityManager) {

    System.out.println("----\nnative query");

    final Query nativeQuery = entityManager.createNativeQuery("select * from Property");
    final List resultList = nativeQuery.getResultList();

    for (final Object o : resultList) {
      if (o.getClass().isArray()) {
        final Object oa[] = (Object[]) o;
        System.out.println("Array as List ..................." + Arrays.asList(oa));
      } else {
        System.out.println("Array is not list .................." + o);
      }
    }
  } // end of queryNative
}
