package jhu.ep.amiller;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name = "RegistrarEntity")
@Table(name = "REGISTRAR")
/**
 * JPA Entity class that represents the Registrar table in our database.
 *
 * @author Adam
 * @date 2021-03-15
 *
 */
public class RegistrarEntity implements Serializable {

  private static final long serialVersionUID = 5254504269934735624L;

  @ManyToOne(fetch = FetchType.LAZY) // mappedBy = "registrar")
  // @JoinColumns({@JoinColumn(name = "Course_ID", referencedColumnName = "Course_ID"),
  // @JoinColumn(name = "User_ID", referencedColumnName = "User_ID")})
  @JoinColumn(name = "Course_ID", referencedColumnName = "Course_ID")
  private CoursesEntity course;

  @Id
  @Column(name = "Course_ID")
  private String courseId;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "User_ID", referencedColumnName = "User_ID")
  private StudentEntity student;



  @Id
  @Column(name = "User_Id")
  private String userId;

  // @ManyToOne // (mappedBy = "student")
  // @JoinColumn(name = "User_ID")
  // private StudentEntity student;


  //
  // public RegistrarEntity() {
  // super();
  // }

  @Override
  public boolean equals(final Object regEnt) {
    return regEnt instanceof RegistrarEntity
        && Objects.equals(getCourseId(), ((RegistrarEntity) regEnt).getCourseId())
        && Objects.equals(getUserId(), ((RegistrarEntity) regEnt).getUserId())
        && Objects.equals(getCourse(), ((RegistrarEntity) regEnt).getCourse())
        && Objects.equals(getStudent(), ((RegistrarEntity) regEnt).getStudent());

  }

  public CoursesEntity getCourse() {
    return this.course;
  }

  public String getCourseId() {
    return this.courseId;
  }

  public StudentEntity getStudent() {
    return this.student;
  }

  public String getUserId() {
    return this.userId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCourseId(), getUserId(), getCourse(), getStudent());
  }

  public void setCourse(final CoursesEntity course) {
    this.course = course;
  }

  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setStudent(final StudentEntity student) {
    this.student = student;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

}
