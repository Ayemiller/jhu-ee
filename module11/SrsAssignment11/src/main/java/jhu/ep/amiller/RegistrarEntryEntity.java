package jhu.ep.amiller;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "RegistrarEntryEntity")
@Table(name = "REGISTRAR")
/**
 * Entity class that represents the Registrar table in our database, to be used in our CMT bean.
 *
 * @author Adam
 * @date 2021-04-13
 *
 */
public class RegistrarEntryEntity implements Serializable {

  private static final long serialVersionUID = 5254504269934735624L;

  @Id
  @Column(name = "Course_ID")
  private String courseId;

  @Id
  @Column(name = "User_Id")
  private String userId;

  @Override
  public boolean equals(final Object regEnt) {
    return regEnt instanceof RegistrarEntryEntity
        && Objects.equals(getCourseId(), ((RegistrarEntryEntity) regEnt).getCourseId())
        && Objects.equals(getUserId(), ((RegistrarEntryEntity) regEnt).getUserId());
  }

  public String getCourseId() {
    return this.courseId;
  }

  public String getUserId() {
    return this.userId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCourseId(), getUserId());
  }


  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

}
