package jhu.ep.amiller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Properties;
import javax.ejb.EJBException;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.MapMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Handler class for the registration and withdraw functions. This class performs the SQL queries
 * for registering and withdrawing from a class in the SRS application. It also performs error
 * checking for the SQL operations and updates the CoursesSupportBean based on the number of
 * students registered as a result of the action.
 *
 * @author Adam Miller (amill189)
 * @date 2021-03-08
 */
public class RegistrationHandler {
  private static final String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
  private static final String INITIAL_CONTEXT_FACTORY =
      "org.wildfly.naming.client.WildFlyInitialContextFactory";
  private static final String PASSWORD = "mqpassword";
  private static final String PROVIDER_URL = "http-remoting://127.0.0.1:8080";
  private static final String REG_COURSE_TOPIC = "RegCourseTopic";
  private static final String USERNAME = "mquser";
  private final int courseCapacity;
  private final CommonQueries queries;
  private boolean useLongRunning = false;
  RegistrarCourseBean regCourseBean;

  /**
   * Constructor
   *
   * @param capacity - the course capacity
   */
  public RegistrationHandler(final Integer capacity) {
    this.courseCapacity = capacity;
    this.queries = new CommonQueries();

    try {
      final Context c = new InitialContext();
      this.regCourseBean = (RegistrarCourseBean) c.lookup(
          "java:global/SrsAssignment11/RegistrarCourseBean!jhu.ep.amiller.RegistrarCourseBean");
    } catch (final NamingException e) {
      e.printStackTrace();
    }

  }

  /**
   * Register to a course.
   *
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if query success, false otherwise
   */
  public boolean register(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {

    return registerForCourse(regBean, coursesBean);
  }

  /**
   * Withdraw from a course.
   *
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if query success, false otherwise
   */
  public boolean withdraw(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        return withdrawFromCourse(con, regBean, coursesBean);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Helper function to return the connection.
   *
   * @return the connection if successful, null otherwise
   * @throws SQLException
   */
  private Connection getConnection() throws SQLException {

    try {
      final DataSource ds;
      final Context jndiDbPathContext = new InitialContext();

      // load jndi DataSource/URL specified in WEB-INF/jboss-web.xml
      ds = (DataSource) jndiDbPathContext.lookup("java:comp/env/jndi/srsDb");
      return ds.getConnection();
    } catch (final NamingException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void produceRegCourseMessage(final String userId, final String courseId,
      final String courseName, final LocalDateTime now) {
    Context namingContext = null;
    final Properties env = new Properties();
    env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
    env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, PROVIDER_URL));
    ConnectionFactory connectionFactory = null;
    try {
      namingContext = new InitialContext(env);
      final String connectionFactoryString =
          System.getProperty("connection.factory", DEFAULT_CONNECTION_FACTORY);
      connectionFactory = (ConnectionFactory) namingContext.lookup(connectionFactoryString);
    } catch (final NamingException e) {
      e.printStackTrace();
    }
    if (connectionFactory != null) {
      try (JMSContext context = connectionFactory.createContext(USERNAME, PASSWORD)) {
        final MapMessage regMessage = context.createMapMessage();
        regMessage.setString("User_ID", userId);
        regMessage.setString("Course_ID", courseId);
        regMessage.setString("Course_Name", courseName);
        regMessage.setString("Date_of_Registration", now.toString());
        final Topic topic = context.createTopic(REG_COURSE_TOPIC);
        context.createProducer().send(topic, regMessage);
      } catch (final Exception e) {
        e.printStackTrace();
      } finally {
        try {
          namingContext.close();
        } catch (final NamingException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Function that calls the register CMT bean
   *
   * @param con - the connection
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if the query succeeds, false otherwise
   */
  private boolean registerForCourse(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    final String courseId = (coursesBean.getCourseTitle() + " ").split(" ")[0];
    final String userId = regBean.getUserId();
    int numberRegistered = this.queries.getNumRegistered(courseId, this.courseCapacity);
    if (this.queries.isStudentRegisteredForCourse(courseId, userId)) {
      regBean.setRegistrationResult(
          "Sorry, you were unable to register from this course since you're already registered for it.");
      return false;
    } else if (numberRegistered < this.courseCapacity
        && !this.queries.isStudentRegisteredForCourse(courseId, userId)) {
      try {
        if (this.useLongRunning) {
          this.regCourseBean.insertLong(courseId, userId);
        } else {
          this.regCourseBean.insertShort(courseId, userId);
        }
        numberRegistered += 1;
        regBean
            .setRegistrationResult("You have been registered to " + coursesBean.getCourseTitle());
        // final String courseName = (coursesBean.getCourseTitle() + " ").split(" ", 2)[1];
        // produceRegCourseMessage(userId, courseId, courseName, LocalDateTime.now());
        return true;
      } catch (final EJBException e) {
        e.printStackTrace();
        regBean.setRegistrationResult(
            "Sorry, the registration to this course failed due to a timeout");
      } catch (final Exception e) {
        e.printStackTrace();
        regBean.setRegistrationResult(
            "Sorry, the registration to this course has been closed based on availability");
      } finally {
        this.useLongRunning = !this.useLongRunning;
        coursesBean.setNumberRegistered(numberRegistered);
      }

      return false;
    }
    regBean.setRegistrationResult(
        "Sorry, registration to this course has been closed based on availability");
    return false;
  }

  /**
   * Function that calls the withdraw SQL query
   *
   * @param con - the connection
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if the query succeeds, false otherwise
   */
  private boolean withdrawFromCourse(final Connection con, final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    final String userId = regBean.getUserId();
    final String courseId = (coursesBean.getCourseTitle() + " ").split(" ")[0];
    int numberRegistered = this.queries.getNumRegistered(courseId, this.courseCapacity);

    if (numberRegistered > 0) {
      final String sql = "DELETE FROM REGISTRAR WHERE Course_ID=? AND User_ID=?";
      try (PreparedStatement statement = con.prepareStatement(sql)) {
        statement.setObject(1, courseId);
        statement.setObject(2, userId);
        final boolean result = statement.executeUpdate() > 0;
        if (result) {
          numberRegistered -= 1;
          regBean.setRegistrationResult(
              "You have been withdrawn from " + coursesBean.getCourseTitle());
        } else {
          regBean.setRegistrationResult("Sorry, you were unable to withdraw from this course");
        }
        coursesBean.setNumberRegistered(numberRegistered);
        return result;
      } catch (final SQLException e) {
        e.printStackTrace();
      }
    }
    return false;
  }

}
