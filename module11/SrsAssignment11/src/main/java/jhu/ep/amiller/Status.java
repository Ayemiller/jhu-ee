package jhu.ep.amiller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
/**
 * Stateless EJB used to query the database to generate the Registration Status Report (RSR), now
 * using the JPA entities {@link CoursesEntity} and {@link RegistrarEntity} for database access.
 *
 * @author Adam
 * @date 2021-03-15
 */
public class Status {
  private static Long courseCapacity;


  /**
   * Default constructor. Also sets the default course capacity variable defined in web.xml.
   */
  public Status() {
    if (courseCapacity == null) {
      try {
        // set max number of login tries to value specified in WEB-INF/web.xml
        final InitialContext context = new InitialContext();
        final String courseCapacityConfig = (String) context.lookup("java:comp/env/CourseCapacity");
        if (courseCapacityConfig != null) {
          Status.courseCapacity = Long.parseLong(courseCapacityConfig);
        }
      } catch (final NumberFormatException | NamingException e) {
        e.printStackTrace();
        // setting default value of 20 students per course if config parameter fails
        Status.courseCapacity = 20l;
      }
    }

  }

  /**
   * Method to create a Map of the data required to create a report of only the classes'
   * registration information from the supplied parameter of classes.
   *
   * @param courses - list of classes that the report must contain
   * @return
   */
  public Map<String, Long> getCourseInformation(final List<String> courses) {
    Map<String, Long> registrationStatusReport;
    if (courses.isEmpty()) {
      // get full registration status report
      registrationStatusReport = getAllStatus();
    } else {
      // get registration status report for selected classes
      final HashMap<String, Long> statusReport = new LinkedHashMap<>();
      for (final String course : courses) {
        statusReport.put(course, getStatus(course));
      }
      registrationStatusReport = statusReport;
    }
    return registrationStatusReport;
  }

  /**
   * Method to create the report information required by the Student Registration Status Report.
   *
   * @param lastName - student's last name
   * @return - list of courses to which the student is registered
   */
  public List<String> getCourseInformationForLastName(final String lastName) {
    final List<String> report = new ArrayList<>();
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
    final EntityManager entityManager = emf.createEntityManager();
    try {
      final List<RegistrarEntity> registered = entityManager.createQuery(
          "SELECT r FROM RegistrarEntity r WHERE r.student.lastName=:lastName ORDER BY r.courseId",
          RegistrarEntity.class).setParameter("lastName", lastName).getResultList();
      for (final RegistrarEntity regEnt : registered) {
        report.add(regEnt.getCourse().getCourseId() + " - " + regEnt.getCourse().getCourseTitle());
      }
    } catch (final Exception e) {
      e.printStackTrace();
    } finally {
      entityManager.close();
    }
    return report;
  }

  /**
   * Method to create a Map of the data required to create a report of all classes' registration
   * information using JPA query.
   *
   * @return map containing course ID and title as the key and the number of registered students as
   *         the value
   */
  private Map<String, Long> getAllStatus() {

    final Map<String, Long> statusReport = new LinkedHashMap<>();
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
    final EntityManager entityManager = emf.createEntityManager();
    try {

      // select all Courses inner joined w/ Registrar rows and populate a List of CoursesEntities
      final List<CoursesEntity> courses = getCoursesUsingCriteriaApi(entityManager);

      final List<Long> numStudents = entityManager.createQuery(
          "SELECT COUNT(r.course) FROM RegistrarEntity r GROUP BY r.courseId ORDER BY r.courseId",
          Long.class).getResultList();
      for (int i = 0; i < courses.size(); i++) {
        statusReport.put(courses.get(i).getCourseId() + " " + courses.get(i).getCourseTitle(),
            numStudents.get(i));
      }

    } catch (final Exception e) {
      e.printStackTrace();
    } finally {
      entityManager.close();
    }
    return statusReport;
  }

  /**
   * Helper method to retrieve the course ID given the full course title.
   *
   * @param title
   * @return
   */
  private String getCourseIdFromTitle(final String title) {
    return (title + " ").split(" ")[0];
  }

  private List<CoursesEntity> getCoursesUsingCriteriaApi(final EntityManager entityManager) {

    final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    final CriteriaQuery<CoursesEntity> criteriaQuery =
        criteriaBuilder.createQuery(CoursesEntity.class);
    final Root<CoursesEntity> courseRoot = criteriaQuery.from(CoursesEntity.class);
    criteriaQuery.groupBy(courseRoot.get("courseId"));
    criteriaQuery.orderBy(criteriaBuilder.asc(courseRoot.get("courseId")));

    final TypedQuery<CoursesEntity> typedQuery1 = entityManager.createQuery(criteriaQuery);
    return typedQuery1.getResultList();
  }

  /**
   * Helper method to return the number of students registered to a given course using a JPA query.
   *
   * @param courseId - the course ID
   * @param courseCapacity - the max capacity
   *
   * @return the number of students registered to a given course
   */
  private Long getNumRegistered(final String courseId) {
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
    final EntityManager entityManager = emf.createEntityManager();
    try {

      final Long numRegistered = entityManager.createQuery(
          "SELECT COUNT(r) FROM RegistrarEntity r WHERE r.courseId=:courseId GROUP BY r.courseId ORDER BY r.courseId",
          Long.class).setParameter("courseId", courseId).getSingleResult();
      return numRegistered;

    } catch (final Exception e) {
      e.printStackTrace();
      // in the event of error, return 0 to indicate the course is empty
      return 0l;
    } finally {
      entityManager.close();
    }
  }

  /**
   * Helper method to retrieve the number of students registered for a class.
   *
   * @param courseTitle - full course title
   * @return
   */
  private Long getStatus(final String courseTitle) {
    final String courseId = getCourseIdFromTitle(courseTitle);
    return getNumRegistered(courseId);
  }

}
