<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>SRS Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/simple.css">
    <jsp:useBean id="coursesSupportBean" class="jhu.ep.amiller.CoursesSupportBean" scope="session" />
    <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationSupportBean" scope="session" />
    <%@page import="jhu.ep.amiller.CoursesSupportBean"%>
    <%@page import="jhu.ep.amiller.RegistrationSupportBean"%>
  </head>
  <body>
    <% 
	  String pageKey = "page";
      pageContext.setAttribute(pageKey, "studentRegStatusReport", PageContext.SESSION_SCOPE); 
    %>
       
  	<h1>Student Registration Status Report</h1>
  	<h3>Enter a student's last name and click on Submit:</h3>
  	
  	<form action="RegistrationController" method="POST">
  		<input type="hidden" id="form" name="form" value="studentRegStatusReport" />
  		<input type="text" name="lastname" required />
  		<br>
  		<input type="submit" value="Submit">
  	</form>
  	<br>
  	<% if (regBean.getStudentRegistrationStatusReport() != null) { %>
  	<% if (regBean.getStudentRegistrationStatusReport().size() >  0) { %>
      <br>
      <h3>The student is registered for the following classes:</h3>
      <table class="status">
      	<tr>
      		<th>Class</th>
      	</tr>
      	<c:forEach items="${regBean.studentRegistrationStatusReport}" var="studentRegistrationStatusReport">
	      	<tr>
	      		<td class="className">${studentRegistrationStatusReport}</td>
	      	</tr>
  		</c:forEach>
      </table>
    <% } else { %>
      <br>
      <h3>No records found.</h3>
    <% } %>
  	<% } %>
  	<br>
  	<p><a class="btn" href="home.jsp"><button type="button">Home</button></a> <a class="btn" href="logout.jsp"><button type="button">Logout</button></a></p>
  </body>
</html>