package com.myservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ejbclient")
public class EjbClientServlet extends HttpServlet {

  private static String _mode = "S";
  private static final String LONG_RUNNING_MODE = "L";
  private static final long serialVersionUID = 5521764958182567668L;
  private static final String SHORT_RUNNING_MODE = "S";

  @EJB
  SingletonBean ejb_slsb;

  public EjbClientServlet() {
    super();
  }

  private int getNumberOfRecords() {
    System.out.println("in getNumberOfRecords");
    return this.ejb_slsb.numberOfRecords();
  }

  private String processTransaction() {

    System.out.println("******************  in EjbClientServlet, processTransaction ()");

    final StringBuilder sb = new StringBuilder();

    final int _loopCount = 1;

    for (int i = 1; i <= 4; i++) {

      String output =
          String.format("<br>--- Starting transaction %d of %d, mode = %s ---<br>", i, 4, _mode);

      sb.append(output);

      // Fetch number of Property records in DB before INSERT
      int numberOfRecords = getNumberOfRecords();

      output = String.format("<br>Number of records before INSERT = %d ", numberOfRecords);

      sb.append(output);

      final String insertRecord = "Loop # " + Integer.toString(i);

      // Run transaction
      try {
        if (_mode.equals(SHORT_RUNNING_MODE)) {

          output = "<br>Inserting new record in short-running mode";

          sb.append(output);

          this.ejb_slsb.insertShort(insertRecord);

        } else {

          output = "<br>Inserting new record in long-running mode";

          sb.append(output);

          this.ejb_slsb.insertLong(insertRecord);

        }
      } catch (final EJBException ex) {
        output = "<br>* An EJBException was encountered *";
        sb.append(output);
      } catch (final Exception ex) {
        output = "<br>* An Exception was encountered * ";
        sb.append(output);
      }

      // Fetch registration count after INSERT
      numberOfRecords = getNumberOfRecords();

      output = String.format("<br> Number of records aftder INSERT = %d", numberOfRecords);

      sb.append(output);

      // Done transaction. Switch mode.

      switchMode();
      output = String.format("<br>Done transaction. Switched mode to %s<br>", _mode);

      sb.append(output);

    }

    return sb.toString();

  } // end of processTransaction method

  private void switchMode() {
    _mode = _mode.equals(SHORT_RUNNING_MODE) ? LONG_RUNNING_MODE : SHORT_RUNNING_MODE;
  }

  // Method to handle GET method request
  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {
    doPost(request, response);
  }

  // Method to handle POST method request
  @Override
  protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
      throws ServletException, IOException {

    System.out.println("****************  About to call processTransaction()");

    final String transactionOutput = processTransaction();

    System.out.println("****************  Back from processTransaction()");

    // Set response content type
    resp.setContentType("text/html");

    final PrintWriter out = resp.getWriter();

    out.println("<html>\n" + "<body bgcolor = \"#f0f0f0\">\n" + "<br>" + transactionOutput
        + "<br>Click <a href='index.html'>here</a> to go to main page</br>" + "</body>"
        + "</html>");
    return;
  }

}

