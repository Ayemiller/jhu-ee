package com.myservlet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "Property")
public class Property {
  @Id
  @GeneratedValue
  private Long id;

  private String key;
  private String value;

  public Long getId() {
    System.out.println(
        "get id = " + Long.toString(this.id) + ".......................!!!!!!!!!!!!!!!!!!!!!!!!!!");
    return this.id;
  }

  public String getKey() {
    System.out
        .println("get key = " + this.key + ".......................!!!!!!!!!!!!!!!!!!!!!!!!!!");
    return this.key;
  }

  public String getValue() {
    System.out
        .println("get value = " + this.value + ".......................!!!!!!!!!!!!!!!!!!!!!!!!!!");
    return this.value;
  }

  public void setId(final Long id) {
    System.out.println(
        "set id = " + Long.toString(id) + ".......................!!!!!!!!!!!!!!!!!!!!!!!!!!");
    this.id = id;
  }

  public void setKey(final String key) {
    System.out.println("set key = " + key + ".......................!!!!!!!!!!!!!!!!!!!!!!!!!!");
    this.key = key;
  }

  public void setValue(final String value) {
    System.out.println(
        "set value = " + value + " ???????????????????.........!!!!!!!!!!!!!!!!!!!!!!!!!!");
    this.value = value;
  }

  @Override
  public String toString() {
    return "Property [ id = " + Long.toString(this.id) + ", key=" + this.key + ", value="
        + this.value + "]";
  }
}
