package com.myservlet;

import java.util.Arrays;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
// import javax.ejb.TransactionAttribute;
// import javax.ejb.TransactionAttributeType;
// import javax.ejb.TransactionManagement;
// import javax.ejb.TransactionManagementType;

@Singleton
@TransactionAttribute(TransactionAttributeType.SUPPORTS)

@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class SingletonBean {

  @PersistenceContext(unitName = "PU")
  private EntityManager em;

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public String insertLong(final String input) throws Exception {

    System.out.println(
        "............ in SingleTonBean insertLong method !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    final int i = numberOfRecords();

    System.out.println("Number of records = " + Integer.toString(i));

    queryNative(this.em);

    try {

      final Property p = new Property();
      p.setKey(input);
      p.setValue(input);

      System.out.println("Property object is instantiated !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

      Thread.sleep(4000); // 4 seconds

      this.em.persist(p);
      this.em.flush();

    } catch (final EJBException e) {

      System.out.println(
          "EJBEXCEPTION !!!!!!!!!!!!!!!!!!! TIMEOUT ...... SO, THROWING Exception(e) !!!!!!!!!!!");
      throw new Exception(e);
    } catch (final Exception e) {

      System.out.println("THROWING EXCEPTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!");

      return "I got Exception";
    }
    return "return from insertLong method";
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  public String insertShort(final String input) throws Exception {

    System.out.println("............ in SingleTonBean insertShort method !!!!!!!!!!!!!!!!!!!");

    final int i = numberOfRecords();

    System.out.println("Number of records = " + Integer.toString(i));

    queryNative(this.em);

    final Property p = new Property();
    p.setKey(input);
    p.setValue(input);

    System.out.println("Property object is instantiated !!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    this.em.persist(p);
    this.em.flush();

    System.out.println("Just did PERSIST and FLUSH !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

    return "return from insertShort method";
  }

  public int numberOfRecords() {
    System.out.println("----\nnative query ...... numberOfRecords");
    final Query nativeQuery = this.em.createNativeQuery("select count(*) from Property");

    return nativeQuery.getSingleResult() != null
        ? Integer.parseInt(nativeQuery.getSingleResult().toString())
        : 0;
  } // end of numberOfRecords

  public void queryNative(final EntityManager entityManager) {

    System.out.println("----\nnative query");

    final Query nativeQuery = entityManager.createNativeQuery("select * from Property");
    final List resultList = nativeQuery.getResultList();

    for (final Object o : resultList) {
      if (o.getClass().isArray()) {
        final Object oa[] = (Object[]) o;
        System.out.println("Array as List ..................." + Arrays.asList(oa));
      } else {
        System.out.println("Array is not list .................." + o);
      }
    }
  } // end of queryNative
}
