package jhu.ep.amiller;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
/**
 * Example spring application that contains a bean.
 *
 * @author Adam
 * @date 2021-03-21
 */
public class Module12Application {

  public static void main(final String[] args) {
    SpringApplication.run(Module12Application.class, args);
  }

  @Bean
  public ApplicationRunner demoBean(final Module12ExampleBean beaner) {
    System.out.println("\n****************************************************************");
    System.out.println("Launching Module12ExampleBean via ApplicationRunner");
    System.out.println("****************************************************************");
    return null;
  }

}
