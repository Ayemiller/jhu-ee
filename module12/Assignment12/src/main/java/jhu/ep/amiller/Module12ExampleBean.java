package jhu.ep.amiller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
/**
 * Example spring bean that overrides methods from the post-initialization and pre-destruction
 * phases.
 *
 * @author Adam
 * @date 2021-03-21
 */
public class Module12ExampleBean
    implements InitializingBean, DisposableBean, BeanNameAware, BeanFactoryAware {

  public Module12ExampleBean() {
    System.out.println("\n****************************************************************");
    System.out.println("Module12ExampleBean::Module12ExampleBean()");
    System.out.println("****************************************************************");
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println("\n****************************************************************");
    System.out.println("(InitializingBean) Module12ExampleBean::afterPropertiesSet()");
    System.out.println("****************************************************************");
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("\n****************************************************************");
    System.out.println("(DisposableBean) Module12ExampleBean::destroy()");
    System.out.println("****************************************************************");
  }

  @Override
  public void setBeanFactory(final BeanFactory beanFactory) throws BeansException {
    System.out.println("\n****************************************************************");
    System.out.println(
        "(BeanFactoryAware) Module12ExampleBean::setBeanFactory(final BeanFactory beanFactory)");
    System.out.println("****************************************************************");
  }

  @Override
  public void setBeanName(final String name) {
    System.out.println("\n****************************************************************");
    System.out.println("(BeanNameAware) Module12ExampleBean::setBeanName(final String name)");
    System.out.println("Set bean name to " + name);
    System.out.println("****************************************************************");
  }

}
