package com.mycompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration; 
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@Configuration 
@EnableAutoConfiguration 
@ComponentScan

public class DemoApplication {

    public static void main(String[] args) {
        
        ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

        System.out.println( "Hello World!" );

        System.out.println("# Beans: " + ctx.getBeanDefinitionCount());
        
        String[] beanNames = ctx.getBeanDefinitionNames();
        
        Arrays.sort(beanNames);

        for (String beanName : beanNames) {
            System.out.println("***" + beanName + " : " + ctx.getBean(beanName).getClass().toString());
        }
    } // end of main method 

} //end of class