# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.mycompany.my-784-springboot-3-demo' is invalid and this project uses 'com.mycompany.my784springboot3demo' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.0.BUILD-SNAPSHOT/maven-plugin/)

