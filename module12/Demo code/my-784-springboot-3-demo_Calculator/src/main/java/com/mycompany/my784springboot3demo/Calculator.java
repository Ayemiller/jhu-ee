package com.mycompany.my784springboot3demo;

import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class Calculator {

    private final Collection<Operation> operations;

    public Calculator(Collection<Operation> operations) {
        this.operations = operations;
    }

    public void calculate(int lhs, int rhs, char op) {

        for (  Operation oper : operations) {
            if (oper.handles(op)) {
                
                System.out.println("Input parameters = " + "....." + Integer.toString(lhs) + "........" + Integer.toString(rhs));

                int result = oper.apply(lhs, rhs);
                System.out.printf("%d %s %d = %s%n", lhs, op, rhs, result);
                return;
            }
        }
        throw new IllegalArgumentException("Unknown operation " + op);
    }
}