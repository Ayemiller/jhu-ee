package com.mycompany.my784springboot3demo;

public interface  Operation {

    int apply(int lhs, int rhs);
    boolean handles(char op);

}