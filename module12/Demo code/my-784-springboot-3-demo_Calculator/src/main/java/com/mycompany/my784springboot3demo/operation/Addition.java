package com.mycompany.my784springboot3demo.operation;

import com.mycompany.my784springboot3demo.Operation;
import org.springframework.stereotype.Component;

@Component
class Addition implements Operation {

    @Override
    public int apply(int lhs, int rhs) {
        System.out.println("Addition = " + Integer.toString(lhs) + " ... " + Integer.toString(rhs) + " ..." + Integer.toString(lhs + rhs));

        return lhs + rhs;
    }

    @Override
    public boolean handles(char op) {
        return '+' == op;
    }
}