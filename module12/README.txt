Assignment 12 - Adam Miller - amill189


INTRO

This assignment serves to be an exploration into the Spring Framework and specifically the
API of Spring Bean. The application demonstrates the following calls from the Spring API:

From Bean Post-Initialization phase:
BeanNameAware::setBeanName()
BeanFactoryAware::setBeanFactory(final BeanFactory beanFactory)
InitializingBean::afterPropertiesSet()

From Pre-Destruction phase:
DisposableBean::destroy()


CONTENTS OF SUBMISSION

Assignment12 - Java web application generated and built with Maven

Assignment12/pom.xml - Maven POM file
Assignment12/src/main/java/jhu/ep/amiller/* - Java source for assignment 12
Assignment12/src/main/java/jhu/ep/amiller/Module12ExampleBean.java
Assignment12/src/main/java/jhu/ep/amiller/Module12Application.java

Assignment12/target/module12-1.0.0.jar - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file


BUILD/RUN/DEPLOY INSTRUCTIONS

    To execute the program using maven, run the following command:
    mvn spring-boot:run
