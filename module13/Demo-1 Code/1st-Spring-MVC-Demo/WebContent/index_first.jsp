<html>
<head>
<title>605.784 Demo of Spring MVC</title>
</head>

<body>
	<br>
	<div style="text-align: center">
		
		<h2>
			First Spring MVC demo application<br> 
		</h2>
		
		<h3>
			<a href="welcome.jsp">Please click here to get Welcome message... </a>
		</h3>
		
		<br>(...look into Spring MVC Controller... @RequestMapping("/welcome"))
	</div>
</body>
</html>