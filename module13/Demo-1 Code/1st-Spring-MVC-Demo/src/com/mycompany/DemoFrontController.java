package com.mycompany;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
public class DemoFrontController {
 
	@RequestMapping("/welcome")
	
	public ModelAndView helloClass() {
 
		String message = "<br><div style='text-align:center;'>"
				+ "<h3>*** Hello 605.784 class ***</h3>"
				+ "<br>This message is set by DemoFrontController.java </div><br><br>";
	
		return new ModelAndView("welcome", "message", message);
	
	}
}	