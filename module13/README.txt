Assignment 13 - Adam Miller - amill189


INTRO


This assignment serves to be an exploration into the Spring Framework and specifically the
Spring MVC module. The application demonstrates a simple web application that performs two
use cases- course registration and user profile updating. A single @Controller is used for
both use cases and a single @Service is used for both use cases as well to act as a "datastore".
The service (RegService.java) also initializes the default data which includes the starting user
profile and a list of five courses. The first course 605.784 is set to contain four students and
the rest of the courses contain five students. Since five students is currently configured to 
be the max number of students enrolled, only the course 605.784 will accept enrollment and after
one enrollment, it will give the "waitlist" response. This configuration can be changed however.
The configurable parameters for this project are located in the application.properties and 
the only two that might need to be changed are maxEnrolled (the maximum number of students
that can be registered to any class) and stateAbbreviations (the list of valid state abbreviations
for the user profile update use case). The user profile updating use case implements all the required
validation specified in the assigment including a zip code with 5 digits, all non-empty fields,
date of birth in the past and a correct format (mm/dd/yyyy), and valid two letter State abbreviations.
The web application server that is used is Tomcat 9 which is pulled in through my spring-boot
configuration. The steps to run the application are at the bottom.

CHALLENGING AND DIFFICULT STEPS


Although it was not required for the assignment, I spent the most time on this assignment
attempting to get two specific things to work. The first was launching the Tomcat 9 server 
using the spring-boot:run command. I had a lot of trouble getting the correct set of dependencies
and servlet context configuration to make it work, but I'm glad that I did since it makes
running the program very quick and easy. The second tricky (but no required) part was to 
figure out how to use an all Java annotation spring-boot and spring-mvc configuration. This 
was mostly done out of curiosity and proved to be a very time-consuming process trying to
figure out how to replicate what was done in the example XML configs. Most of the configuration
takes place in WebConfig.java which contains the @Configuration annotation. As it turns out a lot
of items can be configured through the application.properties as wellsuch as the servlet
context path. Overall, I did not find the actual requirements of the assignment to be too
difficult. Most of all of the validation could be done using various built-in annotations
except for matching a list of States. Although this was pretty easily done by creating a 
custom ConstraintValidator.

CONTENTS OF SUBMISSION

SrsFinal - Java web application generated and built with Maven

SrsFinal/pom.xml - Maven POM file
SrsFinal/src/main/java/jhu/ep/amiller/* - Java source for assignment 13
SrsFinal/src/main/resources/application.properties -    Spring boot properties that also contains
                                                        the max number of students per class and a
                                                        list of valid US States
SrsFinal/src/main/webapp/WEB-INF/messages.properties -  Form validation error messages
SrsFinal/src/main/webapp/WEB-INF/views/*.jsp

SrsFinal/target/finalProject.war - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file


BUILD/RUN/DEPLOY INSTRUCTIONS

    To build and execute the program using maven, run the following command:
    mvn clean package spring-boot:run
    
    Then, navigate your web-browser to http://localhost:8080/finalProject/ to access the app.
