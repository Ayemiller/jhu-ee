package jhu.ep.amiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {WebConfig.class})
/**
 * Launcher class for spring boot application. Run from command line using "mvn spring-boot:run"
 *
 * @author Adam
 *
 */
public class FinalProjectApplication extends SpringBootServletInitializer {
  public static void main(final String[] args) {
    SpringApplication.run(FinalProjectApplication.class, args);
  }

}
