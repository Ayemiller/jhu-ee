package jhu.ep.amiller;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
/**
 * Servlet controller for both use cases of the web app.
 *
 * @author Adam
 *
 */
public class RegController {

  private static Logger log = LoggerFactory.getLogger(RegController.class);
  @Autowired
  public RegService regService;
  @Value("${maxEnrolled}")
  private Integer maxEnrolled;


  @PostMapping(value = "/performRegister")
  public String doRegister(@Valid @ModelAttribute("registerform") final Register reg,
      final BindingResult result) {
    if (result.hasErrors()) {
      return "registerPage";
    }
    final int numRegisteredPlusOne = this.regService.getNumRegistered(reg.getCourse()) + 1;
    if (numRegisteredPlusOne > this.maxEnrolled) {
      return "registerFailed";
    }
    this.regService.incrementNumberRegistered(reg.getCourse());
    log.info("Registered to course: {} ", reg.getCourse());
    return "registerSuccess";
  }

  @PostMapping(value = "/performUpdate")
  public String doUpdate(@Valid @ModelAttribute("userform") final User user,
      final BindingResult result) {
    if (result.hasErrors()) {
      return "updatePage";
    }
    this.regService.setUser(user);
    log.info("Updating user: {}", user);
    return "updateSuccess";
  }

  public RegService getRegService() {
    return this.regService;
  }

  @RequestMapping("/register")
  public String registerCourseView(final Model model) {
    model.addAttribute("registerform", new Register());
    model.addAttribute("courseList", this.regService.getCourses());
    return "registerPage";
  }

  public void setRegService(final RegService regService) {
    this.regService = regService;
  }

  @RequestMapping("/update")
  public String updateUserFormView(final Model model) {
    model.addAttribute("userform", this.regService.getUser());
    return "updatePage";
  }

}
