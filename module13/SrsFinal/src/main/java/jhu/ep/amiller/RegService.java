package jhu.ep.amiller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
/**
 * Service class that is used to initialize and store state for both the "update user" and "course
 * registration" use cases.
 *
 * @author Adam
 *
 */
public class RegService {
  private static Logger log = LoggerFactory.getLogger(RegService.class);

  /* Course number and title as key, number enrolled as value */
  private Map<String, Integer> courses;
  @Resource
  private User user;

  public List<String> getCourses() {
    return new ArrayList<>(this.courses.keySet());
  }

  public Integer getNumRegistered(final String course) {
    return this.courses.get(course);
  }

  public User getUser() {
    return this.user;
  }

  public void incrementNumberRegistered(final String course) {
    this.courses.put(course, this.courses.get(course) + 1);
  }

  @PostConstruct
  public void init() {
    log.info("Constructing default user.");
    this.user = new User("amill189", "Adam", "Miller", new Date(), "113 34th Street", "Chicago",
        "IL", "60642");

    log.info("Adding default courses.");
    this.courses = new LinkedHashMap<>();
    this.courses.put("605.784 - Enterprise Computing with Java", 4);
    this.courses.put("605.785 - Web Services: Frameworks, Processes, Applications", 5);
    this.courses.put("605.786 - Enterprise System Design and Implementation", 5);
    this.courses.put("605.787 - Rich Internet Applications with Ajax", 5);
    this.courses.put("605.788 - Big Data Processing Using Hadoop", 5);
  }

  public void setUser(@Valid final User user) {
    this.user = user;
  }

}
