package jhu.ep.amiller;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import org.springframework.stereotype.Component;

@Component
/**
 * Pojo used to hold the state for the register use case.
 *
 * @author Adam
 *
 */
public class Register implements Serializable {

  private static final long serialVersionUID = 1299573083452129660L;
  @NotEmpty
  private String course;

  public Register() {}

  public Register(final String course) {
    this.course = course;
  }

  public String getCourse() {
    return this.course;
  }

  public void setCourse(final String course) {
    this.course = course;
  }

  @Override
  public String toString() {
    return "Course:" + getCourse();
  }

}
