package jhu.ep.amiller;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = StateValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/**
 * Annotation for US State validation.
 *
 * @author Adam
 *
 */
public @interface State {

  Class<?>[] groups() default {};

  String message() default "{State}";

  Class<? extends Payload>[] payload() default {};
}
