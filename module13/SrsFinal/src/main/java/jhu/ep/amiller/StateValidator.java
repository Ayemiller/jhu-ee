package jhu.ep.amiller;

import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Value;

/**
 * Class to perform validation of states. Pulls in valid "stateAbbreviation" list defined in
 * application.properties
 *
 * @author Adam
 *
 */
public class StateValidator implements ConstraintValidator<State, String> {

  @Value("${stateAbbreviations}")
  private List<String> stateAbbreviations;

  @Override
  public void initialize(final State state) {}

  @Override
  public boolean isValid(final String stateField, final ConstraintValidatorContext cxt) {
    if (stateField == null || "".equals(stateField) || stateField.length() != 2) {
      return false;
    }
    return this.stateAbbreviations.contains(stateField);
  }

}
