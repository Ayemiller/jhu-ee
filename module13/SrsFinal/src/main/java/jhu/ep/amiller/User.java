package jhu.ep.amiller;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Component
public class User implements Serializable {

  private static final long serialVersionUID = 722024198939631280L;

  @NotEmpty
  private String city;

  @NotNull
  @DateTimeFormat(pattern = "MM/dd/yyyy")
  @Past
  private Date dateOfBirth;

  private String firstName; // cannot update
  @NotEmpty
  @State
  private String state;

  @NotEmpty
  private String street;

  private String userId; // cannot update

  @Size(min = 5, max = 5)
  @Digits(integer = 5, fraction = 0)
  @NotEmpty
  private String zip;

  String lastName; // cannot update

  public User() {}

  public User(final String userId, final String firstName, final String lastName,
      final Date dateOfBirth, final String street, final String city, final String state,
      final String zip) {
    this.userId = userId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth;
    this.street = street;
    this.city = city;
    this.state = state;
    this.zip = zip;
  }

  public String getCity() {
    return this.city;
  }

  public Date getDateOfBirth() {
    return this.dateOfBirth;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getState() {
    return this.state;
  }

  public String getStreet() {
    return this.street;
  }

  public String getUserId() {
    return this.userId;
  }


  public String getZip() {
    return this.zip;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public void setDateOfBirth(final Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public void setState(final String state) {
    this.state = state;
  }

  public void setStreet(final String street) {
    this.street = street;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

  public void setZip(final String zip) {
    this.zip = zip;
  }

  @Override
  public String toString() {
    return "User [city=" + this.city + ", dateOfBirth=" + this.dateOfBirth + ", firstName="
        + this.firstName + ", state=" + this.state + ", street=" + this.street + ", userId="
        + this.userId + ", zip=" + this.zip + ", lastName=" + this.lastName + "]";
  }

}
