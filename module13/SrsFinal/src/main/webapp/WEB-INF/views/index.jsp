<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Student Registration System</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">	    
	</head>
	<body>
		<div class="container">
		    <h3 align="center" class="text-primary">Student Registration System</h3>
		    <br><hr />
		    <div>&nbsp;</div>
	    	<br><br>
	    	<a href="register">Register to Course</a>
	        <br><br>
	        <a href="update">User Profile Update</a>
	    </div>
	</body>
</html>