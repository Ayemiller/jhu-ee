<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Register to Course</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <style type="text/css">
        .errormsg {
            color: red;
        }
    </style>
</head>
<body>
	<div class="container">
		<h3 align="center" class="text-primary">Register to Course</h3>
		<br><hr />
	    <div>&nbsp;</div>
    	<br><br>
		<form:form action="/finalProject/performRegister" method="POST" modelAttribute="registerform">
		    		 
			<div class="form-group">
			<label>Course:</label><form:select path="course" items="${courseList}" />
			<small><form:errors path="course" cssClass="errormsg" /></small>
			</div>
			<div class="form-group">
			   <button type="submit" class="btn btn-primary">Register</button>
 			</div>
	   	</form:form>
	   	<br><br>
		<a href="/finalProject"> ...Click here to go back to main page</a>
	</div>
</body>
</html>