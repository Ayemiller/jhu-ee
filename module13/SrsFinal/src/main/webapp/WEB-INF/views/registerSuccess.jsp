<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Registration Success</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">	    
	</head>
	<body>
		<div class="container">
			<h3 align="center" class="text-primary">Register to Course</h3>
		    <br><hr />
		    <div>&nbsp;</div>
	    	<br><br>
	        <h4 align="center">You're successfully registered for ${registerform.course}</h4>
	        <br><br>
	        <br><br>
	        <a href="/finalProject"> ...Click here to go back to main page</a>
	        
	    </div>
	</body>
</html>