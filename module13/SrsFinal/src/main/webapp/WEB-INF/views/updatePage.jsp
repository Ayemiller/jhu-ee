<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Update User Info</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	    <style type="text/css">
	        .errormsg {
	            color: red;
	        }
	    </style>
	</head>
	<body>
		<div class="container">
		    <h3 align="center" class="text-primary">Update User Info</h3>
		    <br><hr />
		    <div>&nbsp;</div>
	    	<br><br>
	    	<form:form action="/finalProject/performUpdate" method="POST" modelAttribute="userform">
	    		 <form:hidden path = "userId" value = "${userform.userId}" />
	    		 <form:hidden path = "firstName" value = "${userform.firstName}" />
	    		 <form:hidden path = "lastName" value = "${userform.lastName}" />
	    		 <div class="form-group">
				    <label>Date of Birth:</label><form:input path="dateOfBirth" size="30" cssClass="form-control" placeholder="mm/dd/yyyy" />
				    <small><form:errors path="dateOfBirth" cssClass="errormsg" /></small>
				 </div>
	    		 <div class="form-group">
	     			<label>Street:</label><form:input path="street" size="30" cssClass="form-control" placeholder="${userform.street}" />
				    <small><form:errors path="street" cssClass="errormsg" /></small>
				 </div>
				 <div class="form-group">
				    <label>City:</label><form:input path="city" size="30" cssClass="form-control" placeholder="${userform.city}" />
				    <small><form:errors path="city" cssClass="errormsg" /></small>
				 </div>
				 <div class="form-group">
				    <label>State:</label><form:input path="state" size="30" cssClass="form-control" placeholder="${userform.state}" />
				    <small><form:errors path="state" cssClass="errormsg" /></small>
				 </div>
				 <div class="form-group">
				    <label>Zip:</label><form:input path="zip" size="30" cssClass="form-control" placeholder="${userform.zip}" />
				    <small><form:errors path="zip" cssClass="errormsg" /></small>
				 </div>
				 <div class="form-group">
				    <button type="submit" class="btn btn-primary">Update</button>
				 </div>
	    	</form:form>
	    	<br><br>
			<a href="/finalProject"> ...Click here to go back to main page</a>
	    </div>
	</body>
</html>