<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
	<head>
		<title>Registration Success</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">	    
	</head>
	<body>
		<div class="container">
			<h3 align="center" class="text-primary">Update User Info</h3>
		    <br><hr />
		    <div>&nbsp;</div>
	    	<br><br>
	    	<h4 align="center" class="text-primary">Your profile was updated successfully!</h4>
			<form:form method="get" action="init" modelAttribute="user">
		        <div class="form-group">
					User Name: ${userform.userId} <br>
					First Name: ${userform.firstName} <br>
					Last Name: ${userform.lastName} <br>
					Date of Birth: ${userform.dateOfBirth} <br>
		     		Street: ${userform.street} <br>
					City: ${userform.city} <br>
					State: ${userform.state} <br>
					Zip: ${userform.zip}
				</div>
			</form:form>
	        	<br><br>
	        	<a href="/finalProject"> ...Click here to go back to main page</a>
    	</div>
	</body>
</html>