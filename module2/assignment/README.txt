Assignment 2 - Adam Miller - amill189



INTRO

The first step of this assignment was the installation of JDK 8 (version 1.8.0_281), 
Maven (version 3.6.3), WildFly (14.0.1.Final), H2 Database (version 1.4.200), Eclipse 
EE (version 2020-12) and Sublime Text 3 (version 3.2.2).

For the coding portion of the assignment, Maven was used to generate a template
of a Java application (maven-archetype-quickstart) and a template of a Java web
application (maven-archetype-webapp). The Java application is named 
"first-java-app" and the Java web application is named "first-web-app". The templates
were slightly modified to demonstrate the ability to modify and build changes.



CONTENTS OF SUBMISSION

first-java-app - console-based Java application generated and built w/ Maven
first-java-app/target/first-java-app-1.0-SNAPSHOT.jar - packaged JAR file
first-java-app/src/main/java/jhu/ep/amiller/App.java - modified source file

first-web-app - browser based Java WAR app generated/built/deployed w/ Maven
first-web-app/target/first-web-app.war - packaged WAR file
first-web-app/src/main/webapp/index.jsp - modified JSP file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file



BUILD/RUN/DEPLOY INSTRUCTIONS

"first-java-app"
- Command to generate with Maven:
	mvn archetype:generate -DgroupId=jhu.ep.amiller -DartifactId=first-java-app -DarchetypeArtifactId=maven-archetype-quickstart
- Command to build with Maven:
	mvn package
- This application runs from the command line with the following command:
	java -cp target/first-java-app-1.0-SNAPSHOT.jar jhu.ep.amiller.App

"first-web-app"
- Command to generate with Maven:
	mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-webapp -DarchetypeVersion=1.3 -DgroupId=org.wildfly -DartifactId=first-web-app
- Command to build with Maven:
	mvn package
- This application is deployed using Maven to the WildFly server with the command:
	mvn wildfly:deploy
	NOTE: the org.wildfly.plugins pluginGroup must be listedn in your Maven settings.xml
- On my machine's WildFly configuration, I could access the web-app using my browser
  at the following URL: http://127.0.0.1:8080/first-web-app/index.jsp

