Assignment 3 - Adam Miller - amill189


INTRO

This assignment involved creating a Student Registration System (SRS) using an MVC
architecture implemented using JSP for the view, a Java servlet for the controller,
and Java beans as the model.

For the coding portion of the assignment, Maven was used to generate a template
of a Java web application (maven-archetype-webapp). The Java web application is named 
"SrsAssignment3".

I added a large amount of error checking of the user's input in my controller and whenever
the controller rejected the user input, the java bean model was used to send a message back
to the view to instruct the user how to correct their input.



CONTENTS OF SUBMISSION

SrsAssignment3 - Java web application generated and built with Maven

SrsAssignment3/pom.xml - Maven POM file

SrsAssignment3/src/main/java/jhu/ep/amiller/RegistrationBean.java - Java bean (model)
SrsAssignment3/src/main/java/jhu/ep/amiller/RegistrationController.java - Java servlet (controller)

SrsAssignment3/src/main/webapp/index.jsp - main SRS page, aka SRS Form A (view)
SrsAssignment3/src/main/webapp/registration_form_b.jsp - second SRS page, aka SRS Form B (view)
SrsAssignment3/src/main/webapp/registration_success.jsp - success SRS page (view)

SrsAssignment3/target/SrsAssignment3.war - packaged WAR file
screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file


BUILD/RUN/DEPLOY INSTRUCTIONS

Command to build with Maven: mvn package

Command to deploy to WildFly server: mvn clean wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment3/ or http://127.0.0.1:8080/SrsAssignment3/index.jsp