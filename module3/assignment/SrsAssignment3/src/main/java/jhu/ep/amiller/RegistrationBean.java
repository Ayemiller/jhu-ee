package jhu.ep.amiller;

/**
 * Java Bean class to serve as the model for our SRS application. It contains getters and setters
 * for all of the fields that will be entered by the user in the JSP view.
 *
 * @author Adam Miller
 * @date 2021-02-15
 */
public class RegistrationBean {
  private String address;
  private String city;
  private String email;
  private String firstName;
  private String lastName;

  /* This field is used to pass error messages from the controller to the view */
  private String message = "";

  private String password;
  private String ssn1;
  private String ssn2;
  private String ssn3;
  private String state;
  private String userId;
  private String zip;

  /**
   * Default constructor
   */
  public RegistrationBean() {
    super();
  }

  public String getAddress() {
    return this.address;
  }

  public String getCity() {
    return this.city;
  }

  public String getEmail() {
    return this.email;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getMessage() {
    return this.message;
  }

  public String getPassword() {
    return this.password;
  }

  public String getSsn1() {
    return this.ssn1;
  }

  public String getSsn2() {
    return this.ssn2;
  }

  public String getSsn3() {
    return this.ssn3;
  }

  public String getState() {
    return this.state;
  }

  public String getUserId() {
    return this.userId;
  }

  public String getZip() {
    return this.zip;
  }

  public void setAddress(final String address) {
    this.address = address;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public void setSsn1(final String ssn1) {
    this.ssn1 = ssn1;
  }

  public void setSsn2(final String ssn2) {
    this.ssn2 = ssn2;
  }

  public void setSsn3(final String ssn3) {
    this.ssn3 = ssn3;
  }

  public void setState(final String state) {
    this.state = state;
  }

  public void setUserId(final String userId) {
    this.userId = userId;
  }

  public void setZip(final String zip) {
    this.zip = zip;
  }

}
