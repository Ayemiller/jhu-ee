package jhu.ep.amiller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/RegistrationController")
/**
 * Controller/Servlet class to be used in the MVC architecture of the SRS registration application.
 * This class performs the business logic and does error checking on the user-provided input sent
 * from the view (jsp).
 *
 * @author Adam Miller (amill189)
 * @date 2021-02-15
 *
 */
public class RegistrationController extends HttpServlet {
  private static final String BEAN_NAME = "regBean";
  private static final long serialVersionUID = -6021101521775588333L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public RegistrationController() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {

    final HttpSession session = request.getSession();
    final ServletContext servletContext = getServletContext();
    RegistrationBean regBean = (RegistrationBean) session.getAttribute(BEAN_NAME);

    // get the attribute to tell us which page the view is currently on
    final String currentPage = (String) request.getSession().getAttribute("page");
    if (currentPage != null && "success".equals(currentPage)) {
      // invalidate the session once registration is complete
      session.invalidate();
    } else if (currentPage != null && "formA".equals(currentPage)) {
      // retrieve, check, and set the fields submitted from "Form A"
      regBean = new RegistrationBean();
      session.setAttribute(BEAN_NAME, regBean);

      // retrieve the values from the user input
      final String userId = request.getParameter("userid");
      final String password = request.getParameter("password");
      final String passwordRepeat = request.getParameter("passwordrepeat");
      final String firstName = request.getParameter("firstname");
      final String lastName = request.getParameter("lastname");
      final String ssn1 = request.getParameter("ssn1");
      final String ssn2 = request.getParameter("ssn2");
      final String ssn3 = request.getParameter("ssn3");
      final String email = request.getParameter("email");

      if ("".equals(userId) || "".equals(password) || "".equals(passwordRepeat)
          || "".equals(firstName) || "".equals(lastName) || "".equals(ssn1) || "".equals(ssn2)
          || "".equals(ssn3) || "".equals(email)) {
        // catch empty fields and send a message to the user
        regBean.setMessage("Please fill out all fields properly before submitting form!");
        final RequestDispatcher rd = servletContext.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
        return;
      } else if (!password.equals(passwordRepeat)) {
        // make sure that the passwords match, if not, prompt to re-enter
        regBean.setMessage("The passwords did not match, please try again");
        final RequestDispatcher rd = servletContext.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
        return;
      }

      // set the values of the model using the java bean
      regBean.setUserId(userId);
      regBean.setPassword(password);
      regBean.setFirstName(firstName);
      regBean.setLastName(lastName);
      regBean.setSsn1(ssn1);
      regBean.setSsn2(ssn2);
      regBean.setSsn3(ssn3);
      regBean.setEmail(email);
      regBean.setMessage("");

      // proceed to registration form "B"
      final RequestDispatcher rd = servletContext.getRequestDispatcher("/registration_form_b.jsp");
      rd.forward(request, response);
    } else if (currentPage != null && "formB".equals(currentPage)) {
      // retrieve, check, and set the fields submitted from "form B"

      // retrieve the values from the user input
      final String address = request.getParameter("address");
      final String city = request.getParameter("city");
      final String state = request.getParameter("state");
      final String zip = request.getParameter("zipcode");

      if ("".equals(address) || "".equals(city) || "".equals(state) || "".equals(zip)) {
        // catch empty fields and send a message to the user
        regBean.setMessage("Please fill out all fields properly before submitting form!");
        final RequestDispatcher rd = servletContext.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);
        return;
      }

      // set the values of the model using the java bean
      regBean.setAddress(address);
      regBean.setCity(city);
      regBean.setState(state);
      regBean.setZip(zip);

      // proceed to registration final success page
      final RequestDispatcher rd = servletContext.getRequestDispatcher("/registration_success.jsp");
      rd.forward(request, response);
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }

}
