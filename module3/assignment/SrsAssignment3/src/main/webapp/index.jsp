<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>SRS Registration Form A</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationBean" scope="session" />
    <%@page import="jhu.ep.amiller.RegistrationBean"%>
  </head>
  <body>
    <%
    String pageKey = "page";
       pageContext.setAttribute(pageKey, "formA", PageContext.SESSION_SCOPE);
    %>
       
  	<h1>SRS Registration</h1>

  	<% if (regBean.getMessage() != null) { %>
      <span style="color: red"><%=regBean.getMessage() %></span><br />
    <% } %>
    <form action="RegistrationController" method="POST">
	    <table>
		    <tr>
		        <td class="label">
		        	User Id:
		        </td>
		        <td>
		        	<input type="text" name="userid" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Password: 
		        </td>
		        <td>
		        	<input type="password" name="password" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Password (repeat):
		        </td>
		        <td>
		        	<input type="password" name="passwordrepeat" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	First name:
		        </td>
		        <td>
		        	<input type="text" name="firstname" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Last name:
		       	</td>
		        <td>
		        	<input type="text" name="lastname" required />
		       	</td>
		    </tr>
		    <tr>
		       	<td class="label">
		        	Social Security Number:
		       	</td>
		       	<td>
		       		<input type="text" name="ssn1" maxlength="3" size="3" max="999" pattern="[0-9][0-9][0-9]" style="width: 4ch" required /> -
		       		<input type="text" name="ssn2" maxlength="2" size="2" max="99" pattern="[0-9][0-9]" style="width: 3ch" required /> -
		       		<input type="text" name="ssn3" maxlength="4" size="4" max="9999" pattern="[0-9][0-9][0-9][0-9]" style="width: 5ch" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Email:
		       	</td>
		      	<td>
		      		<input type="email" name="email" required />
		     	</td>
		    </tr>
		    <tr>
		    	<td colspan="2" class="button">
		        	<input type="submit" value="Continue" />
		        </td>
		    </tr>
		</table>
    </form>
  
  </body>
</html>