<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
 
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SRS Registration Success</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="style.css">
      <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationBean" scope="session" />
      <%@page import="jhu.ep.amiller.RegistrationBean"%>
    </head>
  <body>
    <%
    String pageKey = "page";
       pageContext.setAttribute(pageKey, "success", PageContext.SESSION_SCOPE);
    %>
    <h2>SRS Registration Complete</h2>
    <h4>Thanks for registering ${regBean.firstName}!</h4>
    <h4>Registration Information:</h4>
    ${regBean.firstName} ${regBean.lastName}<br>
    ${regBean.address}<br>
    ${regBean.city}, ${regBean.state} ${regBean.zip}<br><br>
    user id: ${regBean.userId}<br>
    ssn: ${regBean.ssn1}-${regBean.ssn2}-${regBean.ssn3}<br>
    email: ${regBean.email}<br>
  	<br><a href="logout.jsp">...go back</a>
  </body>
</html>