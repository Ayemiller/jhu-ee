Assignment 4 - Adam Miller - amill189


INTRO

This assignment involves adding database connectivity to our SRS website. The system
now supports user registration and login using a WildFly hosted H2 database. The system
uses an MVC architecture implemented using JSP for the view, a Java servlet for the controller,
and Java beans as the model.

I added a large amount of error checking of the user's input in my controller and whenever
the controller rejected the user input, the java bean model was used to send a message back
to the view to instruct the user how to correct their input. The application checks to see
if a user id already exists, and if it does, a message is displayed telling the user to try
a different user id. The same error checking makes sure that the two passwords match and that
all fields are properly filled out as well.

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/srsDb.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/Documents\jhu\java-ee\module4\assignment\srsDb;DB_CLOSE_DELAY=-1;AUTO_SERVER=TRUE
JNDI Name: java:jboss/datasources/srsDb

To login to the included database, use adamtest as both the username and password. 
A fresh database is included in backupSrsDB.zip with the tables created, but with no rows of data.

CONTENTS OF SUBMISSION

SrsAssignment4 - Java web application generated and built with Maven

SrsAssignment4/pom.xml - Maven POM file

SrsAssignment4/src/main/java/jhu/ep/amiller/RegistrationBean.java - Java bean (model)
SrsAssignment4/src/main/java/jhu/ep/amiller/RegistrationController.java - Java servlet (controller)

SrsAssignment4/src/main/webapp/index.jsp - main SRS page, aka SRS Form A (view)
SrsAssignment4/src/main/webapp/logout.jsp - page to support session invalidation (view)
SrsAssignment4/src/main/webapp/registration_form_a.jsp - first SRS page, aka SRS Form A (view)
SrsAssignment4/src/main/webapp/registration_form_b.jsp - second SRS page, aka SRS Form B (view)
SrsAssignment4/src/main/webapp/registration_success.jsp - success SRS page (view)
SrsAssignment4/src/main/webapp/style.css - css stylesheet

SrsAssignment4/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment4/src/main/webapp/WEB-INF/web.xml

SrsAssignment4/target/SrsAssignment4.war - packaged WAR file
screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)
backupSrsDB.zip - backed up SrsDb with tables defined, but no data inserted


BUILD/RUN/DEPLOY INSTRUCTIONS

Command to build with Maven: mvn package

Command to deploy to WildFly server: mvn clean wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment4/ or http://127.0.0.1:8080/SrsAssignment4/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64));
CREATE TABLE REGISTRAR (Course_ID varchar(16), Number_registered int);

To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/srsDb --name=SrsDb --connection-url=jdbc:h2:~/Documents/jhu/java-ee/module4/assignment/SrsDb;DB_CLOSE_DELAY=-1;AUTO_SERVER=TRUE --driver-name=h2

