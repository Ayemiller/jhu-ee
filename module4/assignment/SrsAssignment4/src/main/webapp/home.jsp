<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
 
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SRS Registration Success</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="style.css">
      <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationBean" scope="session" />
      <%@page import="jhu.ep.amiller.RegistrationBean"%>
    </head>
  <body>
    <%
    String pageKey = "page";
       pageContext.setAttribute(pageKey, "success", PageContext.SESSION_SCOPE);
    %>
    <h2>SRS Home</h2>
    <h4>Thanks for logging in ${regBean.firstName} ${regBean.lastName}!</h4>
    <br><a href="javascript:void(0)">Register for Course (coming soon!)</a>
  	<br><a href="logout.jsp">...go back</a>
  </body>
</html>