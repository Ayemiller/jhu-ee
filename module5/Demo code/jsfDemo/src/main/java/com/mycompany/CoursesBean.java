package com.mycompany;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped

public class CoursesBean implements Serializable {

  public static class Course {
    public String courseLabel;
    public String courseValue;

    public Course(final String courseLabel, final String courseValue) {
      this.courseLabel = courseLabel;
      this.courseValue = courseValue;
    }

    public String getCourseLabel() {
      return this.courseLabel;
    }

    public String getCourseValue() {
      return this.courseValue;
    }
  }

  private static Map<String, Object> courseTitleValue;
  static {
    courseTitleValue = new LinkedHashMap<>();

    courseTitleValue.put("605.784 Enterprise Computing with Java",
        "605.784 Enterprise Computing with Java"); // label, value
    courseTitleValue.put("605.785 Web Services: Frameworks, Processes, Applications",
        "605.785 Web Services: Frameworks, Processes, Applications");
    courseTitleValue.put("605.786 Enterprise System Design and Implementation",
        "605.786 Enterprise System Design and Implementation");
    courseTitleValue.put("605.787 Rich Internet Applications with Ajax",
        "605.787 Rich Internet Applications with Ajax");
    courseTitleValue.put("605.788 Big Data Processing Using Hadoop",
        "605.788 Big Data Processing Using Hadoop");
  }

  public String courseTitle;

  public String getcourseTitle() {
    return this.courseTitle;
  }

  public Map<String, Object> getcourseTitleValue() {
    return courseTitleValue;
  }

  public void setcourseTitle(final String courseTitle) {
    this.courseTitle = courseTitle;
  }

}
