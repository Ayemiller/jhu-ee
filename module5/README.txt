Assignment 5 - Adam Miller - amill189


INTRO

This assignment involves adding course registration functionality to our SRS website using JSF.
The system now supports course registration and withdraw using a WildFly hosted H2 database. The 
new functionality in the system uses an MVC architecture implemented with JSF.

The only user input is in the selection of radio buttons and selecting the drop-down list of courses.
If the user tries to register for a course that already has the max number of students (as defined
by the CourseCapacity config parameter in web.xml), an error message is printed to the user. Similar
functionality occurs for trying to withdraw from a course with zero students.

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The web.xml configuration file specifies a parameter, "CourseCapacity", which is retrieved from 
the namespace java:comp/env/CourseCapacity. It is initially set to 10 and if the config file
is absent, a sane default of 20 is used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/H2_784_JNDI.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI

To login to the included database, use adamtest as both the username and password. 
A fresh database is included in backupSrsDB.zip with the tables created, including the following 
sample data:

COURSES
    COURSE_ID  	COURSE_TITLE  
    605.784	    Enterprise Computing with Java
    605.785	    Web Services: Frameworks, Processes, Applications
    605.786	    Enterprise System Design and Implementation
    605.787	    Rich Internet Applications with Ajax
    605.788	    Big Data Processing Using Hadoop

STUDENT
    USER_ID  	PASSWORD  	FIRST_NAME  	LAST_NAME  	SSN  	EMAIL  	ADDRESS  
    adamtest	adamtest	Adam	Miller	123-45-6789	amill189@jh.edu	123 blah street

REGISTRAR
    COURSE_ID  	NUMBER_REGISTERED  
    605.786	    0
    605.787	    0
    605.788	    0
    605.784	    10
    605.785	    10
    
    
CONTENTS OF SUBMISSION

SrsAssignment5 - Java web application generated and built with Maven

SrsAssignment5/pom.xml - Maven POM file
SrsAssignment5/src/main/java/jhu/ep/amiller/* - Java source for assignment 5

SrsAssignment5/src/main/webapp/* - JSP from assignment 4 and JSF (xhtml) added in assignment 5
SrsAssignment5/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment5/src/main/webapp/WEB-INF/web.xml

SrsAssignment5/target/SrsAssignment5.war - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)
backupSrsDB.zip - backed up SrsDb with tables defined, but no data inserted


BUILD/RUN/DEPLOY INSTRUCTIONS

Command to build with Maven: mvn package

Command to deploy to WildFly server: mvn clean wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment5/ or http://127.0.0.1:8080/SrsAssignment5/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64));
CREATE TABLE REGISTRAR (Course_ID varchar(16), Number_registered int);

To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/H2_784_JNDI --name=H2_784_DS --connection-url=jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1 --driver-name=h2
