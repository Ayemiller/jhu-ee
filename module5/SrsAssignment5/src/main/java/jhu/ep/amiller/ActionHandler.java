package jhu.ep.amiller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Logged
@SessionScoped
/**
 * Handler for the two kinds of ActionEvent, withdraw and register.
 *
 * @author Adam
 * @date 2021-03-01
 */
public class ActionHandler implements Serializable {

  private static int courseCapacity;
  private static final Logger logger = Logger.getLogger(ActionHandler.class.getCanonicalName());
  private static final long serialVersionUID = -6254875818905685966L;
  private final RegistrationHandler registrationHandler;

  /**
   * Default constructor. Retrieves the configurable parameters for CourseCapacity and passes it to
   * the RegistrationHandler.
   */
  public ActionHandler() {
    logger.log(Level.INFO, "ActionHandler created.");
    try {
      // set max number of login tries to value specified in WEB-INF/web.xml
      final InitialContext context = new InitialContext();
      final String courseCapacityConfig = (String) context.lookup("java:comp/env/CourseCapacity");
      if (courseCapacityConfig != null) {
        ActionHandler.courseCapacity = Integer.parseInt(courseCapacityConfig);
      }
    } catch (final NumberFormatException | NamingException e) {
      e.printStackTrace();
      // setting default value of 20 students per course if config parameter fails
      ActionHandler.courseCapacity = 20;
    }

    this.registrationHandler = new RegistrationHandler(ActionHandler.courseCapacity);
  }

  /**
   * Handle the register action.
   *
   * @param event - the register event
   */
  public void registerAction(@Observes @Register final ActionEvent event) {
    logger.log(Level.INFO, "ActionHandler - Register Handler: {0}", event);
    // call a specific register handler method
    if (this.registrationHandler.register(event.getRegistrationSupportBean(),
        event.getCoursesSupportBean())) {
      logger.log(Level.INFO, "Created registration record.");
    } else {
      logger.log(Level.INFO, "Unable to create registration record.");
    }

  }

  /**
   * Handle the withdraw action.
   *
   * @param event - the withdraw event
   */
  public void withdrawAction(@Observes @Withdraw final ActionEvent event) {
    logger.log(Level.INFO, "ActionHandler - Withdraw Handler: {0}", event);
    // call the withdraw handler method
    if (this.registrationHandler.withdraw(event.getRegistrationSupportBean(),
        event.getCoursesSupportBean())) {
      logger.log(Level.INFO, "Created withdraw record.");
    } else {
      logger.log(Level.INFO, "Unable to create withdraw record.");
    }
  }

}
