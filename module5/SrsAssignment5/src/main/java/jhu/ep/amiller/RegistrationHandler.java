package jhu.ep.amiller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Handler class for the registration and withdraw functions. This class performs the SQL queries
 * for registering and withdrawing from a class in the SRS application. It also performs error
 * checking for the SQL operations and updates the CoursesSupportBean based on the number of
 * students registered as a result of the action.
 *
 * @author Adam Miller (amill189)
 * @date 2021-03-01
 */
public class RegistrationHandler {

  private final int courseCapacity;

  /**
   * Constructor
   *
   * @param capacity - the course capacity
   */
  public RegistrationHandler(final int capacity) {
    this.courseCapacity = capacity;
  }

  /**
   * Register to a course.
   *
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if query success, false otherwise
   */
  public boolean register(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {

    try (final Connection con = getConnection()) {
      if (con != null) {
        return registerForCourse(con, regBean, coursesBean);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Withdraw from a course.
   *
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if query success, false otherwise
   */
  public boolean withdraw(final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        return withdrawFromCourse(con, regBean, coursesBean);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Helper function to return the connection.
   *
   * @return the connection if successful, null otherwise
   * @throws SQLException
   */
  private Connection getConnection() throws SQLException {

    try {
      final DataSource ds;
      final Context jndiDbPathContext = new InitialContext();

      // load jndi DataSource/URL specified in WEB-INF/jboss-web.xml
      ds = (DataSource) jndiDbPathContext.lookup("java:comp/env/jndi/srsDb");
      return ds.getConnection();
    } catch (final NamingException e) {
      e.printStackTrace();
    }
    return null;

  }


  /**
   *
   * @param con
   * @param courseId
   * @return
   */
  private int getNumRegistered(final Connection con, final String courseId) {

    final String sql = "SELECT Course_ID, Number_registered FROM REGISTRAR WHERE Course_ID=?";
    try (PreparedStatement statement = con.prepareStatement(sql)) {
      statement.setObject(1, courseId);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          return rs.getInt(2);
        }
      } catch (final SQLException e1) {
        e1.printStackTrace();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    // in the event of error, return true to indicate the course is full
    return this.courseCapacity;
  }

  /**
   * Function that calls the register SQL query
   *
   * @param con - the connection
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if the query succeeds, false otherwise
   */
  private boolean registerForCourse(final Connection con, final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    final String courseId = (coursesBean.getCourseTitle() + " ").split(" ")[0];
    int numberRegistered = getNumRegistered(con, courseId);

    if (numberRegistered < this.courseCapacity) {
      final String sql = "UPDATE REGISTRAR SET Number_registered=? WHERE Course_ID=?";
      try (PreparedStatement statement = con.prepareStatement(sql)) {

        statement.setObject(1, numberRegistered + 1);
        statement.setObject(2, courseId);
        final boolean result = statement.executeUpdate() > 0;
        if (result) {
          numberRegistered += 1;
          regBean
              .setRegistrationResult("You have been registered to " + coursesBean.getCourseTitle());
        } else {
          regBean.setRegistrationResult(
              "Sorry, the registration to this course has been closed based on availability");
        }
        coursesBean.setNumberRegistered(numberRegistered);
        return result;
      } catch (final SQLException e) {
        e.printStackTrace();
      }
    }
    regBean.setRegistrationResult(
        "Sorry, the registration to this course has been closed based on availability");
    return false;
  }

  /**
   * Function that calls the withdraw SQL query
   *
   * @param con - the connection
   * @param regBean - the registration support bean
   * @param coursesBean - the courses support bean
   *
   * @return true if the query succeeds, false otherwise
   */
  private boolean withdrawFromCourse(final Connection con, final RegistrationSupportBean regBean,
      final CoursesSupportBean coursesBean) {
    int numberRegistered = getNumRegistered(con, regBean.getCourseId());
    final String courseId = (coursesBean.getCourseTitle() + " ").split(" ")[0];

    if (numberRegistered > 0) {
      final String sql = "UPDATE REGISTRAR SET Number_registered=? WHERE Course_ID=?";
      try (PreparedStatement statement = con.prepareStatement(sql)) {
        statement.setObject(1, numberRegistered - 1);
        statement.setObject(2, courseId);
        final boolean result = statement.executeUpdate() > 0;
        if (result) {
          numberRegistered -= 1;
          regBean.setRegistrationResult(
              "You have been withdrawn from " + coursesBean.getCourseTitle());
        } else {
          regBean.setRegistrationResult("Sorry, you were unable to withdraw from this course");
        }
        coursesBean.setNumberRegistered(numberRegistered);
        return result;
      } catch (final SQLException e) {
        e.printStackTrace();
      }
    }
    return false;
  }

}
