<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>SRS Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/simple.css">
    <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationSupportBean" scope="session" />
    <%@page import="jhu.ep.amiller.RegistrationSupportBean"%>
  </head>
  <body>
    <% 
	  String pageKey = "page";
      pageContext.setAttribute(pageKey, "frontPage", PageContext.SESSION_SCOPE); 
    %>
       
  	<h1>Welcome to the Student Registration Site</h1>
  	<h2>If you have an account, please log in</h2>
  	<% if (regBean.getMessage() != null) { %>
      <span style="color: red"><%=regBean.getMessage() %></span><br />
    <% } %>
  	<form action="RegistrationController" method="POST">
  		<input type="hidden" id="form" name="form" value="login">
	    <table>
		    <tr>
		        <td class="label">
		        	User Id:
		        </td>
		        <td>
		        	<input type="text" name="userid" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Password: 
		        </td>
		        <td>
		        	<input type="password" name="password" required />
		        </td>
		    </tr>
		    <tr>
		    	<td>
		    		<input type="submit" value="Login" />
		    	</td>
		    	<td>
		    		<a class="btn" href="logout.jsp"><button type="button">Reset</button></a>
		    	</td>
		    </tr>
	    </table>
    </form>
  	
	<h2>For new users, please register first</h2>
	<form action="RegistrationController" method="POST">
		<input type="hidden" id="form" name="form" value="register">
		<input type="submit" value="Register" />
	</form>
  
  </body>
</html>