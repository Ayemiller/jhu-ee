<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
 
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SRS Registration Form B</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="css/simple.css">
      <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationSupportBean" scope="session" />
      <%@page import="jhu.ep.amiller.RegistrationSupportBean"%>
    </head>
  <body>
  	<% String pageKey = "page";
       pageContext.setAttribute(pageKey, "formB", PageContext.SESSION_SCOPE); %>
    	
  	<h1>SRS Registration</h1>
  	<% if (regBean.getMessage() != null) { %>
      <span style="color: red"><%=regBean.getMessage() %></span><br />
    <% } %>
    <form action="RegistrationController" method="POST">
    	<input type="hidden" id="form" name="form" value="formB">
	    <table>
		    <tr>
		        <td class="label">
		        	Address:
		        </td>
		        <td>
		        	<input type="text" name="address" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	City, State: 
		        </td>
		        <td>
		        	<input type="text" name="city" required />
		        	,
		        	<select name="state">
						<option value="AL">AL</option>
						<option value="AK">AK</option>
						<option value="AR">AR</option>	
						<option value="AZ">AZ</option>
						<option value="CA">CA</option>
						<option value="CO">CO</option>
						<option value="CT">CT</option>
						<option value="DC">DC</option>
						<option value="DE">DE</option>
						<option value="FL">FL</option>
						<option value="GA">GA</option>
						<option value="HI">HI</option>
						<option value="IA">IA</option>	
						<option value="ID">ID</option>
						<option value="IL">IL</option>
						<option value="IN">IN</option>
						<option value="KS">KS</option>
						<option value="KY">KY</option>
						<option value="LA">LA</option>
						<option value="MA">MA</option>
						<option value="MD">MD</option>
						<option value="ME">ME</option>
						<option value="MI">MI</option>
						<option value="MN">MN</option>
						<option value="MO">MO</option>	
						<option value="MS">MS</option>
						<option value="MT">MT</option>
						<option value="NC">NC</option>	
						<option value="NE">NE</option>
						<option value="NH">NH</option>
						<option value="NJ">NJ</option>
						<option value="NM">NM</option>			
						<option value="NV">NV</option>
						<option value="NY">NY</option>
						<option value="ND">ND</option>
						<option value="OH">OH</option>
						<option value="OK">OK</option>
						<option value="OR">OR</option>
						<option value="PA">PA</option>
						<option value="RI">RI</option>
						<option value="SC">SC</option>
						<option value="SD">SD</option>
						<option value="TN">TN</option>
						<option value="TX">TX</option>
						<option value="UT">UT</option>
						<option value="VT">VT</option>
						<option value="VA">VA</option>
						<option value="WA">WA</option>
						<option value="WI">WI</option>	
						<option value="WV">WV</option>
						<option value="WY">WY</option>
					</select>	
		        </td>
		    </tr>
		    <tr>
		    	<td class="label">
		        	Zip/Postal Code:
		        </td>
		        <td>
		        	<input type="text" name="zipcode" maxlength="5" size="5" max="99999" pattern="[0-9][0-9][0-9][0-9][0-9]" title="Please enter a 5-digit zipcode" required />
		        </td>
		    </tr>
		    <tr>
		    	<td class="button">
		        	<input type="submit" value="Submit" />
		        </td>
		    	<td class="button">
		    		<a class="btn" href="logout.jsp"><button type="button">Reset</button></a>
		    	</td>
		    </tr>
		</table>
    </form>
  </body>
</html>