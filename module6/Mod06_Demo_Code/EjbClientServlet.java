package com.myservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import javax.ejb.EJB;	

import com.mycompany.TextProcessorBean;

@WebServlet("/ejbclient")

public class EjbClientServlet extends HttpServlet {

        @EJB  TextProcessorBean ejb_slsb;

     public EjbClientServlet () {
        super();
     }
 
    // Method to handle POST method request.
   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

      doPost(request, response);

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        System.out.println( "result from EJB = " + ejb_slsb.doIt("leonidfelikson"));


      // Set response content type
      resp.setContentType("text/html");

      PrintWriter out = resp.getWriter();

      out.println(
         "<html>\n" +
            "<body bgcolor = \"#f0f0f0\">\n" +
               "<br>" +
                  "Length is " + ejb_slsb.doIt(req.getParameter("input")) +
               "</br>Click <a href='index.html'>here</a> to go to main page</br>" +
            "</body>" +
         "</html>"
      );

        
    } 
}








