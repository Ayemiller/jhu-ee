package com.mycompany;

import javax.ejb.Stateless;

@Stateless 

public class TextProcessorBean {

      public int doIt (String text) {

              /* Business method implementation code */

              return text.length();

      }
}
