Assignment 6 - Adam Miller - amill189


INTRO

This assignment involves adding registration status report (RSR) functionality to our SRS website 
using a stateless EJB. The system now supports displaying an RSR for one or more classes using an 
EJB to handle the calls to the WildFly hosted H2 database.

The only user input is in the selection of the checkboxes of classes displayed on the RSR page.
If the user does not select any classes on the page, a report containing all classes is displayed.

One feature that I added in this iteration was the use of a WebFilter named AuthFilter. This filter
is used to prevent any access to pages in the site if a user has not first authenticated. If this
is detected, the user will be redirected to the login page and a message will be displayed 
informing the user that "You must login before accessing any other pages."

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The web.xml configuration file specifies a parameter, "CourseCapacity", which is retrieved from 
the namespace java:comp/env/CourseCapacity. It is initially set to 10 and if the config file
is absent, a sane default of 20 is used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/H2_784_JNDI.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI

To login to the included database, use adamtest as both the username and password. 
A fresh database is included in backupSrsDB.zip with the tables created, including the following 
sample data:

COURSES
    COURSE_ID  	COURSE_TITLE  
    605.784	    Enterprise Computing with Java
    605.785	    Web Services: Frameworks, Processes, Applications
    605.786	    Enterprise System Design and Implementation
    605.787	    Rich Internet Applications with Ajax
    605.788	    Big Data Processing Using Hadoop

STUDENT
    USER_ID  	PASSWORD  	FIRST_NAME  	LAST_NAME  	SSN  	EMAIL  	ADDRESS  
    adamtest	adamtest	Adam	Miller	123-45-6789	amill189@jh.edu	123 blah street

REGISTRAR
    COURSE_ID  	NUMBER_REGISTERED  
    605.784     10
    605.785     10
    605.786	    0
    605.787	    0
    605.788	    0

    
CONTENTS OF SUBMISSION

SrsAssignment6 - Java web application generated and built with Maven

SrsAssignment6/pom.xml - Maven POM file
SrsAssignment6/src/main/java/jhu/ep/amiller/* - Java source for assignment 6

SrsAssignment6/src/main/webapp/* - JSP from assignment 4 and JSF (status.jsp added in assignment 6)
SrsAssignment6/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment6/src/main/webapp/WEB-INF/web.xml

SrsAssignment6/target/SrsAssignment6.war - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)
backupSrsDB.zip - backed up SrsDb with tables defined, but no data inserted


BUILD/RUN/DEPLOY INSTRUCTIONS

Command to build with Maven: mvn package

Command to deploy to WildFly server: mvn clean wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment6/ or http://127.0.0.1:8080/SrsAssignment6/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64));
CREATE TABLE REGISTRAR (Course_ID varchar(16), Number_registered int);

To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/H2_784_JNDI --name=H2_784_DS --connection-url=jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1 --driver-name=h2
