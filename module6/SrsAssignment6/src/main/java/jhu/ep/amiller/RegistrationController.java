package jhu.ep.amiller;


import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/RegistrationController")
/**
 * Controller/Servlet class to be used in the MVC architecture of the SRS registration application.
 * This class performs the business logic and does error checking on the user-provided input sent
 * from the view (jsp).
 *
 * Supports registering students and logging into the SRS system by incorporating an H2 database
 * that is defined in jboss-web.xml. This class executes prepared statements to retrieve and store
 * the student information into the database through JNDI.
 *
 * Servlet now has the additional functionality of providing a Registration Status Report (RSR)
 * which lets a logged-in user create a report by querying the database on classes and the number of
 * students enrolled. If no classes are selected, the report contains information on all classes.
 *
 * @author Adam Miller (amill189)
 * @date 2021-03-08
 *
 */
public class RegistrationController extends HttpServlet {
  private static final String BEAN_NAME = "regBean";
  private static final String LOGIN_PAGE = "/index.jsp";
  private static int numTriesAllowed;
  private static final String REGISTRATION_FORM_A = "/registration_form_a.jsp";


  private static final long serialVersionUID = -6021101521775588333L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public RegistrationController() {
    super();
  }

  @Override
  /**
   * Attempts to load the configuration parameter 'numTries' from web.xml and sets the default value
   * to 3 if a failure occurs.
   */
  public void init() {
    try {
      // set max number of login tries to value specified in WEB-INF/web.xml
      final InitialContext context = new InitialContext();
      final String numTriesConfig = (String) context.lookup("java:comp/env/numTries");
      if (numTriesConfig != null) {
        RegistrationController.numTriesAllowed = Integer.parseInt(numTriesConfig);
      }

    } catch (final NumberFormatException | NamingException e) {
      e.printStackTrace();
      // setting default value of 3 login tries if config parameter fails
      RegistrationController.numTriesAllowed = 3;
    }

  }

  /**
   * Helper function to authenticate and return the model bean containing the authenticated student
   *
   * @param userId - the user's id
   * @param password - the user's password
   * @return the model of the authenticated student, or null if authentication fails
   */
  private RegistrationSupportBean attemptAuthentication(final String userId, final String password,
      final RegistrationSupportBean regBean) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        return executeAuthenticateQuery(con, userId, password, regBean);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Helper function to insert a student into the student table.
   *
   * @param regBean - model bean containing the student to insert
   * @return true if the insert succeeds, false otherwise
   */
  private boolean attemptInsert(final RegistrationSupportBean regBean) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        return executeStudentDbInsert(con, regBean);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Helper function to return whether or not a user exists.
   *
   * @param userId - the user id
   * @return true if user exists, false otherwise
   */
  private boolean doesUserExist(final String userId) {
    try (final Connection con = getConnection()) {
      if (con != null) {
        return executeUserExistsQuery(con, userId);
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Retrieve a student from the Student table given a user id and password. Then assigns the
   * returned info to the model bean and returns the information.
   *
   * @param con - the datasource connection
   * @param userId - a user's ID
   * @param password - a user's password
   * @return the retrieved information stored in the model bean, otherwise null if row not found
   */
  private RegistrationSupportBean executeAuthenticateQuery(final Connection con,
      final String userId, final String password, final RegistrationSupportBean loginBean) {
    final String sql =
        "SELECT User_ID, Password, First_Name, Last_Name, Email, Address FROM STUDENT WHERE User_ID=? and Password=?";
    try (PreparedStatement statement = con.prepareStatement(sql)) {
      statement.setObject(1, userId);
      statement.setObject(2, password);
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          if (rs.getString(1).equals(userId) && rs.getString(2).equals(password)) {
            loginBean.setFirstName(rs.getString(3));
            loginBean.setLastName(rs.getString(4));
            loginBean.setEmail(rs.getString(5));
            loginBean.setAddress(rs.getString(6));
            return loginBean;
          }
        }
      } catch (final SQLException e1) {
        e1.printStackTrace();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Add a student into student table.
   *
   * @param con - datasource connection
   * @param regBean - model bean used to source data to insert
   * @return true if insert succeeds, false otherwise
   */
  private boolean executeStudentDbInsert(final Connection con,
      final RegistrationSupportBean regBean) {

    final String sql =
        "INSERT INTO STUDENT (User_ID, Password, First_Name, Last_Name, SSN, Email, Address) values(?,?,?,?,?,?,?)";
    try (final PreparedStatement statement = con.prepareStatement(sql)) {
      statement.setObject(1, regBean.getUserId());
      statement.setObject(2, regBean.getPassword());
      statement.setObject(3, regBean.getFirstName());
      statement.setObject(4, regBean.getLastName());
      statement.setObject(5, regBean.getSsn1() + '-' + regBean.getSsn2() + '-' + regBean.getSsn3());
      statement.setObject(6, regBean.getEmail());
      statement.setObject(7, regBean.getAddress());
      statement.executeUpdate();
      return true;
    } catch (final SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Perform query to check if a user exists.
   *
   * @param con - datasource connection
   * @param userId - the user id to check for
   * @return - true if user exists, false otherwise
   */
  private boolean executeUserExistsQuery(final Connection con, final String userId) {
    final String sql = "SELECT User_ID FROM STUDENT WHERE User_ID=?";
    try (PreparedStatement statement = con.prepareStatement(sql)) {
      statement.setObject(1, userId);
      try (ResultSet rs = statement.executeQuery()) {
        return rs.next();
      } catch (final SQLException e1) {
        e1.printStackTrace();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Get a Connection to the database specified in jboss-web.xml
   *
   * @return connection if successful, null if failure.
   * @throws SQLException
   */
  private Connection getConnection() throws SQLException {

    try {
      final DataSource ds;
      final Context jndiDbPathContext = new InitialContext();
      // load jndi DataSource/URL specified in WEB-INF/jboss-web.xml
      ds = (DataSource) jndiDbPathContext.lookup("java:comp/env/jndi/srsDb");
      return ds.getConnection();
    } catch (final NamingException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Helper method to create and display registration status report using logic contained in the
   * Status EJB to query the database.
   *
   * @param request - the request
   * @param response - the response
   * @param servletContext - the servlet context
   * @param regBean - the Registration Support Bean
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleCreateRegStatusReport(final HttpServletRequest request,
      final HttpServletResponse response, final ServletContext servletContext,
      final RegistrationSupportBean regBean) throws ServletException, IOException {
    final Status status = new Status();
    final String[] coursesArray = request.getParameterValues("courses");
    final List<String> courses =
        Arrays.asList(Optional.ofNullable(coursesArray).orElse(new String[0]));
    final Map<String, Integer> report = status.getCourseInformation(courses);
    if (regBean != null) {
      regBean.setRegistrationStatusReport(report);
    }
    handleRegStatusReport(request, response, servletContext);
  }

  /**
   * Helper method used by the controller to handle submission of form A.
   *
   * @param request - the request
   * @param response - the response
   * @param regBean - model bean
   * @param servletContext - servlet's context
   * @param session - the http session
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleFormA(final HttpServletRequest request, final HttpServletResponse response,
      final RegistrationSupportBean regBean, final ServletContext servletContext,
      final HttpSession session) throws ServletException, IOException {
    regBean.setLoginAttempts(0);
    session.setAttribute(BEAN_NAME, regBean);

    // retrieve the values from the user input
    final String userId = request.getParameter("userid");
    final String password = request.getParameter("password");
    final String passwordRepeat = request.getParameter("passwordrepeat");
    final String firstName = request.getParameter("firstname");
    final String lastName = request.getParameter("lastname");
    final String ssn1 = request.getParameter("ssn1");
    final String ssn2 = request.getParameter("ssn2");
    final String ssn3 = request.getParameter("ssn3");
    final String email = request.getParameter("email");


    if ("".equals(userId) || "".equals(password) || "".equals(passwordRepeat)
        || "".equals(firstName) || "".equals(lastName) || "".equals(ssn1) || "".equals(ssn2)
        || "".equals(ssn3) || "".equals(email)) {
      // catch empty fields and send a message to the user
      regBean.setMessage("Please fill out all fields properly before submitting form!");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    } else if (userId.length() > 16 || userId.length() < 8) {
      // make sure that the user ID is between 8 and 16 characters
      regBean.setMessage(
          "The User Id must be at least 8 characters and no more than 16 characters, please try again");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    } else if (password.length() > 16 || password.length() < 8) {
      // make sure that the password is between 8 and 16 characters
      regBean.setMessage(
          "The password must be at least 8 characters and no more than 16 characters, please try again");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    } else if (userId.contains(" ") || password.contains(" ")) {
      // make sure that the user ID is between 8 and 16 characters
      regBean.setMessage("The User Id and passwords may not contain spaces, please try again");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    } else if (!password.equals(passwordRepeat)) {
      // make sure that the passwords match, if not, prompt to re-enter
      regBean.setMessage("The passwords did not match, please try again");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    } else if (doesUserExist(userId)) {
      regBean.setMessage("The User Id already exists, please try a different one.");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
      rd.forward(request, response);
      return;
    }

    // set the values of the model using the java bean
    regBean.setUserId(userId);
    regBean.setPassword(password);
    regBean.setFirstName(firstName);
    regBean.setLastName(lastName);
    regBean.setSsn1(ssn1);
    regBean.setSsn2(ssn2);
    regBean.setSsn3(ssn3);
    regBean.setEmail(email);
    regBean.setMessage("");

    // proceed to registration form "B"
    final RequestDispatcher rd = servletContext.getRequestDispatcher("/registration_form_b.jsp");
    rd.forward(request, response);

  }

  /**
   * Helper method used by the controller to handle submission of form b
   *
   * @param request - the request
   * @param response - the response
   * @param regBean - the model bean
   * @param servletContext - servlet's context
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleFormB(final HttpServletRequest request, final HttpServletResponse response,
      final RegistrationSupportBean regBean, final ServletContext servletContext)
      throws ServletException, IOException {
    // retrieve the values from the user input
    final String address = request.getParameter("address");
    final String city = request.getParameter("city");
    final String state = request.getParameter("state");
    final String zip = request.getParameter("zipcode");

    if ("".equals(address) || "".equals(city) || "".equals(state) || "".equals(zip)) {
      // catch empty fields and send a message to the user
      regBean.setMessage("Please fill out all fields properly before submitting form!");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(LOGIN_PAGE);
      rd.forward(request, response);
      return;
    }

    // set the values of the model using the java bean
    regBean.setAddress(address);
    regBean.setCity(city);
    regBean.setState(state);
    regBean.setZip(zip);

    // attempt to insert into database
    if (attemptInsert(regBean)) {

      // proceed to registration final success page
      final RequestDispatcher rd = servletContext.getRequestDispatcher("/registration_success.jsp");
      rd.forward(request, response);
    } else {
      // registration failed, go back to home and display failure message
      regBean.setMessage(
          "Unable to register. If you already have an account please login. Otherwise, attempt to re-register.");
      final RequestDispatcher rd = servletContext.getRequestDispatcher(LOGIN_PAGE);
      rd.forward(request, response);
    }
  }

  /**
   * Helper method used by the controller to handle login to SRS
   *
   * @param request - the request
   * @param response - the response
   * @param regBean - model bean
   * @param session - the http session
   * @param servletContext - the servlet's context
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleLogin(final HttpServletRequest request, final HttpServletResponse response,
      final RegistrationSupportBean regBean, final HttpSession session,
      final ServletContext servletContext) throws ServletException, IOException {

    // retrieve the user id and password and process the login attempt
    final String userId = request.getParameter("userid");
    final String password = request.getParameter("password");

    final RegistrationSupportBean loginBean = attemptAuthentication(userId, password, regBean);
    if (loginBean != null) {
      loginBean.setUserId(userId);
      session.setAttribute(BEAN_NAME, loginBean);
      // reset the login attempts counter
      final RequestDispatcher rd = servletContext.getRequestDispatcher("/home.jsp");
      request.setAttribute("registrationSupportBean", loginBean);
      session.setAttribute("userId", loginBean.getUserId());

      rd.forward(request, response);
    } else {
      // increment the number of incorrect logins and print error msg to user
      regBean.setLoginAttempts(regBean.getLoginAttempts() + 1);
      if (regBean.getLoginAttempts() == RegistrationController.numTriesAllowed) {
        session.invalidate();
        regBean.setLoginAttempts(0);
      } else {
        regBean.setMessage(
            "Wrong UserID and/or Password. Please try to login again or register a new account. Login attempt "
                + regBean.getLoginAttempts() + " of " + RegistrationController.numTriesAllowed);
      }
      final RequestDispatcher rd = servletContext.getRequestDispatcher(LOGIN_PAGE);
      rd.forward(request, response);
    }
  }

  /**
   * Helper method used by the controller to register to the SRS
   *
   * @param regBean - model bean
   * @param request - the request
   * @param response - the response
   * @param servletContext - the servlet context
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleRegister(final RegistrationSupportBean regBean,
      final HttpServletRequest request, final HttpServletResponse response,
      final ServletContext servletContext) throws ServletException, IOException {
    regBean.setMessage("");
    regBean.setLoginAttempts(0);
    final RequestDispatcher rd = servletContext.getRequestDispatcher(REGISTRATION_FORM_A);
    rd.forward(request, response);
  }

  /**
   * Helper function to load the RSR page.
   *
   * @param request - the request
   * @param response - the response
   * @param servletContext - the servlet context
   *
   * @throws ServletException
   * @throws IOException
   */
  private void handleRegStatusReport(final HttpServletRequest request,
      final HttpServletResponse response, final ServletContext servletContext)
      throws ServletException, IOException {
    final RequestDispatcher rd = servletContext.getRequestDispatcher("/status.jsp");
    rd.forward(request, response);
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {

    final HttpSession session = request.getSession();
    final ServletContext servletContext = getServletContext();
    final RegistrationSupportBean regBean =
        (RegistrationSupportBean) session.getAttribute(BEAN_NAME);

    // get the hidden parameter to tell us which page the view is currently on
    final String form = request.getParameter("form");
    if (form != null) {
      try {
        if (form.equals("register")) {
          // handle an attempt to register which will redirect to form-A
          handleRegister(regBean, request, response, servletContext);
        } else if (form.equals("login")) {
          // attempt to login to SRS
          handleLogin(request, response, regBean, session, servletContext);
        } else if ("formA".equals(form)) {
          // retrieve, check, and set the fields submitted from "Form A"
          handleFormA(request, response, regBean, servletContext, session);
        } else if ("formB".equals(form)) {
          // retrieve, check, and set the fields submitted from "form B"
          handleFormB(request, response, regBean, servletContext);
        } else if ("goToRegStatusReport".equals(form)) {
          regBean.setRegistrationStatusReport(null);
          handleRegStatusReport(request, response, servletContext);
        } else if ("regStatusReport".equals(form)) {
          handleCreateRegStatusReport(request, response, servletContext, regBean);
        } else {
          session.invalidate();
        }
      } catch (ServletException | IOException e) {
        e.printStackTrace();
        session.invalidate();
      }
    } else {
      session.invalidate();
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }

}
