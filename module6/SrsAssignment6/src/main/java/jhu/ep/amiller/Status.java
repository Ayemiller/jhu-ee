package jhu.ep.amiller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Stateless
/**
 * Stateless EJB used to query the database to generate the Registration Status Report (RSR).
 *
 * @author Adam
 * @date 2021-03-08
 */
public class Status {
  private static Integer courseCapacity;
  private final CommonQueries queries;

  /**
   * Default constructor. Also sets the default course capacity variable defined in web.xml.
   */
  public Status() {
    if (courseCapacity == null) {
      try {
        // set max number of login tries to value specified in WEB-INF/web.xml
        final InitialContext context = new InitialContext();
        final String courseCapacityConfig = (String) context.lookup("java:comp/env/CourseCapacity");
        if (courseCapacityConfig != null) {
          Status.courseCapacity = Integer.parseInt(courseCapacityConfig);
        }
      } catch (final NumberFormatException | NamingException e) {
        e.printStackTrace();
        // setting default value of 20 students per course if config parameter fails
        Status.courseCapacity = 20;
      }
    }
    this.queries = new CommonQueries();
  }

  /**
   * Method to create a Map of the data required to create a report of all classes' registration
   * information.
   *
   * @return
   */
  public Map<String, Integer> getAllStatus() {
    final Map<String, Integer> statusReport = new LinkedHashMap<>();
    final CoursesSupportBean coursesSupportBean = new CoursesSupportBean();
    for (final String course : coursesSupportBean.getCourseTitleValue().keySet()) {
      statusReport.put(course, getStatus(course));
    }
    return statusReport;
  }

  /**
   * Method to create a Map of the data required to create a report of only the classes'
   * registration information from the supplied parameter of classes.
   *
   * @param courses - list of classes that the report must contain
   * @return
   */
  public Map<String, Integer> getCourseInformation(final List<String> courses) {
    Map<String, Integer> registrationStatusReport;
    if (courses.isEmpty()) {
      // get full registration status report
      registrationStatusReport = getAllStatus();
    } else {
      // get registration status report for selected classes
      final HashMap<String, Integer> statusReport = new LinkedHashMap<>();
      for (final String course : courses) {
        statusReport.put(course, getStatus(course));
      }
      registrationStatusReport = statusReport;
    }
    return registrationStatusReport;
  }

  /**
   * Helper method to retrieve the course ID given the full course title.
   *
   * @param title
   * @return
   */
  private String getCourseIdFromTitle(final String title) {
    return (title + " ").split(" ")[0];
  }

  /**
   * Helper method to retrieve the number of students registered for a class.
   *
   * @param courseTitle - full course title
   * @return
   */
  private Integer getStatus(final String courseTitle) {
    final String courseId = getCourseIdFromTitle(courseTitle);
    return this.queries.getNumRegistered(courseId, Status.courseCapacity);
  }


}
