<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
 
  <html>
    <head>
      <meta charset="UTF-8">
      <title>SRS Registration Success</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="css/simple.css">
      <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationSupportBean" scope="session" />
      <%@page import="jhu.ep.amiller.RegistrationSupportBean"%>
    </head>
  <body>
    <%
    String pageKey = "page";
       pageContext.setAttribute(pageKey, "success", PageContext.SESSION_SCOPE);
    %>
    <h2>SRS Home</h2>
    <h4>Thanks for logging in ${regBean.firstName} ${regBean.lastName}!</h4>
    
    <p><a class="btn" href="home.xhtml"><button type="button">Course Registration</button></a></p>
    <form action="RegistrationController" method="POST">
    	<input type="hidden" id="form" name="form" value="goToRegStatusReport" />
    	<input type="submit" value="Registration Status Report" />
    </form>
    <!-- <p><a class="btn" href="home.xhtml"><button type="button">Course Registration</button></a></p>
    <p><a class="btn" href="status.jsp"><button type="button">Registration Status Report</button></a></p>
    -->
  	<br><a href="logout.jsp">...go back</a>
  </body>
</html>