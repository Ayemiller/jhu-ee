<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>SRS Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/simple.css">
    <jsp:useBean id="coursesSupportBean" class="jhu.ep.amiller.CoursesSupportBean" scope="session" />
    <jsp:useBean id="regBean" class="jhu.ep.amiller.RegistrationSupportBean" scope="session" />
    <%@page import="jhu.ep.amiller.CoursesSupportBean"%>
    <%@page import="jhu.ep.amiller.RegistrationSupportBean"%>
  </head>
  <body>
    <% 
	  String pageKey = "page";
      pageContext.setAttribute(pageKey, "regStatusReport", PageContext.SESSION_SCOPE); 
    %>
       
  	<h1>Registration Status Report</h1>
  	<h3>Check any specific course(s) and click on Submit:</h3>
  	
  	<form action="RegistrationController" method="POST">
  		<input type="hidden" id="form" name="form" value="regStatusReport" />
  		<c:forEach items="${coursesSupportBean.courseTitleValue}" var="courseTitleValue">
  			<input type="checkbox" class="regStatusReport" name="courses" value="${courseTitleValue.key}" ><label>${courseTitleValue.key}</label><br>
  		</c:forEach>
  		<br>
  		<input type="submit" value="Submit">
  	</form>
  	<br>
  	
  	<% if (regBean.getRegistrationStatusReport() != null && regBean.getRegistrationStatusReport().size() >  0) { %>
      <br>
      <h3>Report for selected chosen classes:</h3>
      <table class="status">
      	<tr>
      		<th>Class</th>
      		<th class="numStudents">Number Enrolled</th>
      	</tr>
      	<c:forEach items="${regBean.registrationStatusReport}" var="registrationStatusReport">
	      	<tr>
	      		<td class="className">${registrationStatusReport.key}</td>
	      		<td class="numStudents">${registrationStatusReport.value}</td>
	      	</tr>
  		</c:forEach>
      </table>
    <% } %>
  	
  	<br>
  	<p><a class="btn" href="home.jsp"><button type="button">Home</button></a> <a class="btn" href="logout.jsp"><button type="button">Logout</button></a></p>
  </body>
</html>