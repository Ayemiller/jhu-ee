package jhu.ep.amiller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COURSES")
/**
 * JPA Entity class that represents the Courses table in our database.
 *
 * @author Adam
 * @date 2021-03-15
 *
 */
public class CoursesEntity {
  @Id
  @Column(name = "Course_ID")
  private String courseId;

  @Column(name = "Course_Title")
  private String courseTitle;

  @OneToOne
  @JoinColumn(name = "Course_ID")
  private RegistrarEntity registrar;

  public CoursesEntity() {
    super();
  }

  public String getCourseId() {
    return this.courseId;
  }

  public String getCourseTitle() {
    return this.courseTitle;
  }

  public RegistrarEntity getRegistrar() {
    return this.registrar;
  }

  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setCourseTitle(final String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public void setRegistrar(final RegistrarEntity registrar) {
    this.registrar = registrar;
  }

  @Override
  public String toString() {
    return this.courseId + " - " + this.courseTitle;
  }

}
