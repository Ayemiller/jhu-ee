package jhu.ep.amiller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REGISTRAR")
/**
 * JPA Entity class that represents the Registrar table in our database.
 *
 * @author Adam
 * @date 2021-03-15
 *
 */
public class RegistrarEntity {

  @OneToOne(mappedBy = "registrar")
  private CoursesEntity course;

  @Id
  @Column(name = "Course_ID")
  private String courseId;

  @Column(name = "Number_registered")
  private Integer numberRegistered;

  public RegistrarEntity() {
    super();
  }

  public CoursesEntity getCourse() {
    return this.course;
  }

  public String getCourseId() {
    return this.courseId;
  }

  public Integer getNumberRegistered() {
    return this.numberRegistered;
  }

  public void setCourse(final CoursesEntity course) {
    this.course = course;
  }

  public void setCourseId(final String courseId) {
    this.courseId = courseId;
  }

  public void setNumberRegistered(final Integer numberRegistered) {
    this.numberRegistered = numberRegistered;
  }

}
