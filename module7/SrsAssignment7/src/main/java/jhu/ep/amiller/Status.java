package jhu.ep.amiller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Stateless
/**
 * Stateless EJB used to query the database to generate the Registration Status Report (RSR), now
 * using the JPA entities {@link CoursesEntity} and {@link RegistrarEntity} for database access.
 *
 * @author Adam
 * @date 2021-03-15
 */
public class Status {
  private static Integer courseCapacity;

  /**
   * Default constructor. Also sets the default course capacity variable defined in web.xml.
   */
  public Status() {
    if (courseCapacity == null) {
      try {
        // set max number of login tries to value specified in WEB-INF/web.xml
        final InitialContext context = new InitialContext();
        final String courseCapacityConfig = (String) context.lookup("java:comp/env/CourseCapacity");
        if (courseCapacityConfig != null) {
          Status.courseCapacity = Integer.parseInt(courseCapacityConfig);
        }
      } catch (final NumberFormatException | NamingException e) {
        e.printStackTrace();
        // setting default value of 20 students per course if config parameter fails
        Status.courseCapacity = 20;
      }
    }

  }

  /**
   * Method to create a Map of the data required to create a report of only the classes'
   * registration information from the supplied parameter of classes.
   *
   * @param courses - list of classes that the report must contain
   * @return
   */
  public Map<String, Integer> getCourseInformation(final List<String> courses) {
    Map<String, Integer> registrationStatusReport;
    if (courses.isEmpty()) {
      // get full registration status report
      registrationStatusReport = getAllStatus();
    } else {
      // get registration status report for selected classes
      final HashMap<String, Integer> statusReport = new LinkedHashMap<>();
      for (final String course : courses) {
        statusReport.put(course, getStatus(course));
      }
      registrationStatusReport = statusReport;
    }
    return registrationStatusReport;
  }

  /**
   * Method to create a Map of the data required to create a report of all classes' registration
   * information using JPA query.
   *
   * @return map containing course ID and title as the key and the number of registered students as
   *         the value
   */
  private Map<String, Integer> getAllStatus() {

    final Map<String, Integer> statusReport = new LinkedHashMap<>();
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    final EntityManager entityManager = emf.createEntityManager();
    try {

      // select all Courses inner joined w/ Registrar rows and populate a List of CoursesEntities
      final List<CoursesEntity> courses =
          entityManager.createQuery("SELECT r.course FROM RegistrarEntity r ORDER BY r.courseId",
              CoursesEntity.class).getResultList();

      for (final CoursesEntity course : courses) {
        statusReport.put(course.getCourseId() + " " + course.getCourseTitle(),
            course.getRegistrar().getNumberRegistered());
      }

    } catch (final Exception e) {
      e.printStackTrace();
    } finally {
      entityManager.close();
    }
    return statusReport;
  }

  /**
   * Helper method to retrieve the course ID given the full course title.
   *
   * @param title
   * @return
   */
  private String getCourseIdFromTitle(final String title) {
    return (title + " ").split(" ")[0];
  }

  /**
   * Helper method to return the number of students registered to a given course using a JPA query.
   *
   * @param courseId - the course ID
   * @param courseCapacity - the max capacity
   *
   * @return the number of students registered to a given course
   */
  private Integer getNumRegistered(final String courseId, final Integer courseCapacity) {
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    final EntityManager entityManager = emf.createEntityManager();
    try {

      final RegistrarEntity regEntity = entityManager.createQuery(
          "SELECT r FROM RegistrarEntity r WHERE r.courseId=:courseId ORDER BY r.courseId",
          RegistrarEntity.class).setParameter("courseId", courseId).getSingleResult();
      return regEntity.getNumberRegistered();

    } catch (final Exception e) {
      e.printStackTrace();
      // in the event of error, return true to indicate the course is full
      return courseCapacity;
    } finally {
      entityManager.close();
    }
  }

  /**
   * Helper method to retrieve the number of students registered for a class.
   *
   * @param courseTitle - full course title
   * @return
   */
  private Integer getStatus(final String courseTitle) {
    final String courseId = getCourseIdFromTitle(courseTitle);
    return getNumRegistered(courseId, Status.courseCapacity);
  }

}
