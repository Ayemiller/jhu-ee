package jhu.ep.amiller;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
/**
 * JPA Entity class that represents the Person entity for the table per subclass use case.
 *
 * @author Adam
 * @date 2021-03-22
 *
 */
public class AllStudentJoined {
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  private List<GraduateStudentJoined> graduateStudents = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL)
  private List<UndergradStudentJoined> undergradStudents = new ArrayList<>();

  public List<GraduateStudentJoined> getGraduateStudents() {
    return this.graduateStudents;
  }

  public List<UndergradStudentJoined> getUndergradStudents() {
    return this.undergradStudents;
  }

  public void setGraduateStudents(final List<GraduateStudentJoined> graduateStudents) {
    this.graduateStudents = graduateStudents;
  }

  public void setUndergradStudents(final List<UndergradStudentJoined> undergradStudents) {
    this.undergradStudents = undergradStudents;
  }
}
