package jhu.ep.amiller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Class to demonstrate the functionality of the table per subclass (joined) strategy.
 *
 * @author Adam
 * @date 2021-03-22
 */
public class ConcreteDemo {

  public static void main(final String[] args) {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    EntityManager entityManager = emf.createEntityManager();

    try {

      final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<UndergradStudentConcrete> criteria =
          builder.createQuery(UndergradStudentConcrete.class);
      criteria.from(UndergradStudentConcrete.class);
      final List<UndergradStudentConcrete> resultList =
          entityManager.createQuery(criteria).getResultList();

      System.out.println(resultList);
    } finally {
      entityManager.close();
      emf.close();
    }

    emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    entityManager = emf.createEntityManager();

    try {

      final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<PersonConcrete> criteria = builder.createQuery(PersonConcrete.class);
      criteria.from(PersonConcrete.class);
      final List<PersonConcrete> resultList = entityManager.createQuery(criteria).getResultList();

      System.out.println(resultList);
    } finally {
      entityManager.close();
      emf.close();
    }
  }

}
