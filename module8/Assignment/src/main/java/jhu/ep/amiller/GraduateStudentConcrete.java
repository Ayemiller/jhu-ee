package jhu.ep.amiller;

import javax.persistence.Entity;

@Entity(name = "GraduateStudentConcrete")
/**
 * JPA Entity class that represents the Graduate Student entity for the table per subclass (joined)
 * use case.
 *
 * @author Adam
 * @date 2021-03-22
 *
 */
public class GraduateStudentConcrete extends PersonConcrete {

  public String getEmail() {
    return this.email;
  }

  public String getLevel() {
    return this.level;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setLevel(final String level) {
    this.level = level;
  }

  @Override
  public String toString() {
    return super.toString() + "\nEmail: " + getEmail() + "\nLevel: " + getLevel() + "\n";
  }

}
