package jhu.ep.amiller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "PersonId")
// @Table(name = "COURSES")
/**
 * JPA Entity class that represents the Graduate Student entity for the table per subclass (joined)
 * use case.
 *
 * @author Adam
 * @date 2021-03-22
 *
 */
public class GraduateStudentJoined extends PersonJoined {
  @Column(name = "Email")
  private String email;
  @Column(name = "Id")
  private Long id;
  @Column(name = "Level")
  private String level;

  public String getEmail() {
    return this.email;
  }

  public Long getId() {
    return this.id;
  }

  public String getLevel() {
    return this.level;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public void setLevel(final String level) {
    this.level = level;
  }

  @Override
  public String toString() {
    return super.toString() + "\nEmail: " + getEmail() + "\nLevel: " + getLevel() + "\n";
  }

}
