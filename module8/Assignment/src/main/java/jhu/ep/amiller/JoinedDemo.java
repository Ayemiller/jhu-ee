package jhu.ep.amiller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Class to demonstrate the functionality of the table per subclass (joined) strategy.
 *
 * @author Adam
 * @date 2021-03-22
 */
public class JoinedDemo {

  public static void main(final String[] args) {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    EntityManager entityManager = emf.createEntityManager();

    try {

      final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<UndergradStudentJoined> criteria =
          builder.createQuery(UndergradStudentJoined.class);
      criteria.from(UndergradStudentJoined.class);
      final List<UndergradStudentJoined> resultList =
          entityManager.createQuery(criteria).getResultList();

      System.out.println(resultList);
    } finally {
      entityManager.close();
      emf.close();
    }


    emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    entityManager = emf.createEntityManager();
    try {

      final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<GraduateStudentJoined> criteria =
          builder.createQuery(GraduateStudentJoined.class);
      criteria.from(GraduateStudentJoined.class);
      final TypedQuery<GraduateStudentJoined> query = entityManager.createQuery(criteria);
      final List<GraduateStudentJoined> resultList = query.getResultList();
      System.out.println(resultList);
    } finally {
      entityManager.close();
      emf.close();
    }


  }

}
