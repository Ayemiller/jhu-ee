package jhu.ep.amiller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// @Table(name = "COURSES")
/**
 * JPA Entity class that represents the Person entity for the table per subclass use case.
 *
 * @author Adam
 * @date 2021-03-22
 *
 */
public abstract class PersonConcrete {
  @Column(name = "Address")
  private String address;

  @Column(name = "First_Name")
  private String firstName;

  @Column(name = "Last_Name")
  private String lastName;
  @Column(name = "Email")
  protected String email;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  protected Long id;
  @Column(name = "Level")
  protected String level;

  public String getAddress() {
    return this.address;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setAddress(final String address) {
    this.address = address;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  @Override
  public String toString() {
    return "Name: " + getFirstName() + " " + getLastName() + "\nAddress: " + getAddress();
  }

}
