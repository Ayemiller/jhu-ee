package jhu.ep.amiller;

import static org.junit.Assert.assertEquals;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.junit.Before;
import org.junit.Test;

/**
 * Class to demonstrate the functionality of the table per subclass (joined) strategy.
 *
 * @author Adam
 * @date 2021-03-22
 */
public class StrategiesTest {

  List<GraduateStudentConcrete> graduateConcrete;
  List<GraduateStudentJoined> graduateJoined;

  List<PersonConcrete> personConcrete;
  List<PersonJoined> personJoined;

  List<UndergradStudentConcrete> undergradConcrete;
  List<UndergradStudentJoined> undergradJoined;

  @Before
  public void setupEntities() {
    this.undergradConcrete = loadUndergradConcrete();
    this.graduateConcrete = loadGraduateConcrete();

    this.undergradJoined = loadUndergradJoined();
    this.graduateJoined = loadGraduateJoined();

    this.personJoined = loadPersonJoined();
    this.personConcrete = loadPersonConcrete();
  }

  @Test
  public void testNumberOfResultsInGraduateConcrete() {
    assertEquals(3, this.graduateConcrete.size());
  }

  @Test
  public void testNumberOfResultsInGraduateJoined() {
    assertEquals(3, this.graduateJoined.size());
  }

  @Test
  public void testNumberOfResultsInPersonConcrete() {
    assertEquals(6, this.personConcrete.size());
  }

  @Test
  public void testNumberOfResultsInPersonJoined() {
    assertEquals(5, this.personJoined.size());
  }

  @Test
  public void testNumberOfResultsInUndergradConcrete() {
    assertEquals(3, this.undergradConcrete.size());
  }

  @Test
  public void testNumberOfResultsInUndergradJoined() {
    assertEquals(3, this.undergradJoined.size());
  }

  private <T> List<T> loadEntityClass(final Class<T> clazz) {
    final EntityManagerFactory emf = Persistence.createEntityManagerFactory("srsPersistenceUnit");
    final EntityManager entityManager = emf.createEntityManager();
    try {
      final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
      final CriteriaQuery<T> criteria = builder.createQuery(clazz);
      criteria.from(clazz);
      final List<T> resultList = entityManager.createQuery(criteria).getResultList();
      // System.out.println(resultList);
      return resultList;
    } catch (final Exception e) {
      return null;
    } finally {
      entityManager.close();
      emf.close();
    }
  }

  private List<GraduateStudentConcrete> loadGraduateConcrete() {
    return loadEntityClass(GraduateStudentConcrete.class);
  }

  private List<GraduateStudentJoined> loadGraduateJoined() {
    return loadEntityClass(GraduateStudentJoined.class);
  }

  private List<PersonConcrete> loadPersonConcrete() {
    return loadEntityClass(PersonConcrete.class);
  }

  private List<PersonJoined> loadPersonJoined() {
    return loadEntityClass(PersonJoined.class);
  }

  private List<UndergradStudentConcrete> loadUndergradConcrete() {
    return loadEntityClass(UndergradStudentConcrete.class);
  }

  private List<UndergradStudentJoined> loadUndergradJoined() {
    return loadEntityClass(UndergradStudentJoined.class);
  }

}
