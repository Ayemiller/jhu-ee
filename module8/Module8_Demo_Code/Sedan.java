package com.mycompany;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Sedan")
public class Sedan extends Car {
 
private int engine;
 
	public int getEngine() {
		return engine;
	}
 
	public void setEngine(int engine) {
		this.engine = engine;
	}
 
}