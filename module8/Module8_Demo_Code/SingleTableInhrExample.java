package com.mycompany;

import javax.persistence.*;

import com.mycompany.Car;
import com.mycompany.Sedan;
import com.mycompany.Truck;
import com.mycompany.Suv;
import com.mycompany.Van;

public class SingleTableInhrExample {

    public static void main(String[] args) {

        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("testPersistenceUnit");
        EntityManager em = emf.createEntityManager();
     
     	EntityTransaction transaction = em.getTransaction();
		transaction.begin();
        
		Sedan acuratl = new Sedan();
		acuratl.setEngine(275);
		acuratl.setName("ACURA_TL");
		em.persist(acuratl);

		Van pilot = new Van();
		pilot.setNoOfPass(11);
		em.persist(pilot);

		Suv bmw = new Suv();
		bmw.setNoOfDoors(22);
		em.persist(bmw);

    	Truck truck = new Truck();
		truck.setCargo(54);
		em.persist(truck);

    	transaction.commit();

    }

}

