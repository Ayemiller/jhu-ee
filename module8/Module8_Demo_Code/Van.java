package com.mycompany;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Van")
public class Van extends Car {
 
	private int noOfPass;
 
	public int getNoOfPass() {
		return noOfPass;
	}
 
	public void setNoOfPass(int noOfPass) {
		this.noOfPass = noOfPass;
	}
 
}