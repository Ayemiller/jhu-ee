Assignment 8 - Adam Miller - amill189


INTRO

This assignment steps away from SRS and has us developing JPA based
queries to demonstrate inheritence. Two separate use cases have been
implemented demonstrated inheritence between Person and Student. One
for Table per Concrete Class and one for Table per Subclass (Joined).

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI


The provided database contains the following data.

GraduateStudentConcrete
ID      FIRST_NAME      LAST_NAME   ADDRESS     EMAIL   LEVEL  
4   Adam    Miller  123 Fake Street amill189@jh.edu Graduate
5   Charles Cleveland   333 Locust Street   cc223@jh.edu    Graduate
6   David   Davidson    444 Pine Street dd123@jh.edu    Graduat

UndergradStudentConcrete
ID      FIRST_NAME      LAST_NAME   ADDRESS             EMAIL               LEVEL  
1       Adam            Miller      123 Fake Street     amill189@jh.edu     Undergraduate
2       Alex            Anderson    111 Chestnut Street aa123@jh.edu        Undergraduate
3       Bob             Banks       222 Walnut Street   bb456@jh.edu        Undergraduate

PersonJoined
PERSONID    FIRST_NAME      LAST_NAME   ADDRESS  
    1       Adam            Miller      123 Fake Street
    2       Alex            Anderson    111 Chestnut Street
    3       Bob             Banks       222 Walnut Street
    4       Charles         Cleveland   333 Locust Street
    5       David           Davidson    444 Pine Street

GraduateStudentJoined
ID      PERSONID    EMAIL           LEVEL  
1       1           amill189@jh.edu Graduate
2       4           cc223@jh.edu    Graduate
3       5           dd123@jh.edu    Graduate


UndergradStudentJoined
SELECT * FROM UNDERGRADSTUDENTJOINED;
ID      PERSONID    EMAIL           LEVEL  
1       1           amill189@jh.edu Undergraduate
2       2           aa123@jh.edu    Undergraduate
3       3           bb456@jh.edu    Undergraduate
CONTENTS OF SUBMISSION

Assignment/ - Java app(s) and junit test generated and built with Maven

Assignment/pom.xml - Maven POM file
Assignment/src/main/java/jhu/ep/amiller/* - Java source for assignment 8
Assignment/src/test/java/jhu/ep/amiller/* - JUnit test for assignment 8
Assignment/src/main/webapp/META-INF/persistence.xml - defines the JPA/Hibernate properties

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)


BUILD/RUN/DEPLOY INSTRUCTIONS

To run the samples "JoinedDemo" and "ConcreteDemo" use the following Maven commands:
mvn compile exec:java -Dexec.mainClass="jhu.ep.amiller.JoinedDemo"
mvn compile exec:java -Dexec.mainClass="jhu.ep.amiller.ConcreteDemo"

To run the provided JUnit tests, run the following Maven command:
mvn clean test

CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:

# table per concrete class

CREATE TABLE UndergradStudentConcrete (id BIGINT, First_Name varchar(64), Last_Name varchar(64), Address varchar(64), Email varchar(64), Level varchar(64));
CREATE TABLE GraduateStudentConcrete (id BIGINT, First_Name varchar(64), Last_Name varchar(64), Address varchar(64), Email varchar(64), Level varchar(64));

INSERT INTO UndergradStudentConcrete (id, First_Name, Last_Name, Address, Email, Level) VALUES
(1, 'Adam', 'Miller', '123 Fake Street', 'amill189@jh.edu', 'Undergraduate'),
(2, 'Alex', 'Anderson', '111 Chestnut Street', 'aa123@jh.edu', 'Undergraduate'),
(3, 'Bob', 'Banks', '222 Walnut Street', 'bb456@jh.edu', 'Undergraduate');

INSERT INTO GraduateStudentConcrete (id, First_Name, Last_Name, Address, Email, Level) VALUES
(4, 'Adam', 'Miller', '123 Fake Street', 'amill189@jh.edu', 'Graduate'), 
(5, 'Charles', 'Cleveland', '333 Locust Street', 'cc223@jh.edu', 'Graduate'),
(6, 'David', 'Davidson', '444 Pine Street', 'dd123@jh.edu', 'Graduate');


# table per subclass (joined)

CREATE TABLE PersonJoined (PersonId BIGINT IDENTITY, First_Name varchar(64), Last_Name varchar(64), Address varchar(64));
CREATE TABLE UndergradStudentJoined (id BIGINT IDENTITY, PersonId BIGINT, Email varchar(64), Level varchar(64), FOREIGN KEY (PersonId) references PersonJoined(PersonId));
CREATE TABLE GraduateStudentJoined (id BIGINT IDENTITY, PersonId BIGINT, Email varchar(64), Level varchar(64), FOREIGN KEY (PersonId) references PersonJoined(PersonId));

INSERT INTO PersonJoined (First_Name, Last_Name, Address) VALUES
('Adam', 'Miller', '123 Fake Street'),
('Alex', 'Anderson', '111 Chestnut Street'),
('Bob', 'Banks', '222 Walnut Street'),
('Charles', 'Cleveland', '333 Locust Street'),
('David', 'Davidson', '444 Pine Street');

INSERT INTO UndergradStudentJoined (PersonId, Email, Level) VALUES
(1, 'amill189@jh.edu', 'Undergraduate'),
(2, 'aa123@jh.edu', 'Undergraduate'),
(3, 'bb456@jh.edu', 'Undergraduate');

INSERT INTO GraduateStudentJoined (PersonId, Email, Level) VALUES
(1, 'amill189@jh.edu', 'Graduate'),
(4, 'cc223@jh.edu', 'Graduate'),
(5, 'dd123@jh.edu', 'Graduate');
