package com.mycompany;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Address {
                
    @Id
    private int id;
    
    private String addr;

    @OneToOne
    private Student student;

    public int getId() {
        return id;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return id == address.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
 
    @Override
    public String toString() {    


        StringBuilder builder = new StringBuilder();
        builder.append ("Address {id=").append(id).append(", address = ").append(addr).append("}");
    
        return builder.toString();
    }    
}   
    
