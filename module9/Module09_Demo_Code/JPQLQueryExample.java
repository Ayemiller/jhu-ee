package com.mycompany;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class JPQLQueryExample {

   private static EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "testQueryUnit" );      

   public static void main( String[ ] args ) {
   
      try {

          countStudents();
          findAllStudents();
          findByNamedQueries();
          findAllStudents_with_Criteria();

      } finally {
          emfactory.close();
      }

   } //end of main

  private static void findAllStudents() {

      EntityManager entitymanager = emfactory.createEntityManager();

      System.out.println("-- All Students --");

      Query query = entitymanager.createQuery(
              "SELECT e FROM Student e");
  
      List<Student> list = query.getResultList();

      for(Student s:list) {
         System.out.println("Student :" + s);
      }

  }

    private static void findByNamedQueries() {

      EntityManager entitymanager = emfactory.createEntityManager();

      System.out.println("-- Find by Named Queries --");

      Query query = entitymanager.createNamedQuery("Student.findAll");
  
      List<Student> list = query.getResultList();

      for(Student s:list) {
         System.out.println("Student :" + s);
      }

  }

  private static void countStudents() {

      EntityManager entitymanager = emfactory.createEntityManager();

      System.out.println("-- Total number of Students by NamedQuery --");


      Query query = entitymanager.createNamedQuery("Student.count");

      long count = (long) query.getSingleResult();

      System.out.println("Total number of registered students is = " + Long.toString(count));
   
  }

  private static void findAllStudents_with_Criteria(){

      EntityManager entitymanager = emfactory.createEntityManager();

      System.out.println("-- All Students obtained using Criteria API");

      CriteriaBuilder criteriaBuilder = entitymanager.getCriteriaBuilder();
      CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery();
      Root<Student> stud = criteriaQuery.from(Student.class);

     //select all records
     System.out.println("-- Select all Students records");

     CriteriaQuery<Object> select = criteriaQuery.select(stud);
     TypedQuery<Object> typedQuery = entitymanager.createQuery(select);
     List<Object> resultlist = typedQuery.getResultList();
 
     for(Object s:resultlist) {
     //   s = (Student)s;
        System.out.println("Student :" + s);
     }

     //Ordering the records 
     System.out.println ("Select all Students records by follow ordering");

     CriteriaQuery<Object> select1 = criteriaQuery.select(stud);
     select1.orderBy(criteriaBuilder.desc(stud.get("fname")));
     TypedQuery<Object> typedQuery1 = entitymanager.createQuery(select);
     List<Object> resultlist1 = typedQuery1.getResultList();

     for(Object s:resultlist1){
        System.out.println("Student :" + s);
     }

     entitymanager.close( );

  } // end findAllStudents_with_Criteria() 

} 