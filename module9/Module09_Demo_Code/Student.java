package com.mycompany;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;  
import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity

@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s ORDER BY s.fname"),
    @NamedQuery(name = "Student.count",   query = "SELECT COUNT(*) FROM Student s")
})


public class Student {
        
    @Id
    private int id;
    
    private String fname;
    private String lname;
    private String major;

    @OneToOne
    private Address address;
    
    public int getId() {
        return id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return id == student.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Student {" +
                " id = " + id + " fname = "  + fname + " lname = " + lname + " address = " + address.getAddr() + 
                '}';
    }
}


