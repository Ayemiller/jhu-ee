Assignment 7 - Adam Miller - amill189


INTRO

This assignment involves adding a student registration status report (SRSR) functionality.
This function appears as a button after logging in on the "Home" page, along with "Course
Registration" and "Registration Status Report". Adding this functionality involved changing
the database schema, so that specific students' registration to their courses could be mapped
in the database. I accomplished this by removing "NumStudents" from the REGISTRAR table and
adding "User_ID" which is a foreign key to the "User_ID" in the STUDENT table. The logic to
calculate the removed NumStudents is now simply done by querying the REGISTRAR table and 
getting a COUNT(*). All existing queries were modified and tested to support the database 
changes required by this assignment.

The bulk of the functionality to create the SRSR was done using JPQL queries. However, the
Criteria API was used for a query to select all of the courses which can be found in 
Status::getCoursesUsingCriteriaApi. The other new functionality with JPQL queries can be 
found in Status::getCourseInformationForLastName.

The only user input in the SRSR is for the user to enter the last name of the student for 
a which a report should be generated. To test, feel free to enter the last names of "Test" 
or "test2" for the two reports that I've included with the database. The SRSR also properly
handles error checking for last names that are not contained in the database.

The web.xml configuration file specifies a parameter, "numTries", which is retrieved from the
namespace java:comp/env/numTries. It is initially set to 5 and if the configuration file is
absent, a sane default of 3 will be used.

The web.xml configuration file specifies a parameter, "CourseCapacity", which is retrieved from 
the namespace java:comp/env/CourseCapacity. It is initially set to 10 and if the config file
is absent, a sane default of 20 is used.

The jboss-web.xml configuration file specifies a parameter "jndi/srsDb" which defines the JNDI
path and URL for the database, which is currently set to java:jboss/datasources/H2_784_JNDI.

In my environment, the WildFly data source is set to the following connection info:
Connection URL: jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1
JNDI Name: java:jboss/datasources/H2_784_JNDI

To login to the page, use adamtest as both the username and password. 
The included database contains the following sample data:

COURSES
    COURSE_ID  	COURSE_TITLE  
    605.784	    Enterprise Computing with Java
    605.785	    Web Services: Frameworks, Processes, Applications
    605.786	    Enterprise System Design and Implementation
    605.787	    Rich Internet Applications with Ajax
    605.788	    Big Data Processing Using Hadoop

STUDENT
    USER_ID  	PASSWORD  	FIRST_NAME  LAST_NAME  	SSN  	    EMAIL  	         ADDRESS  
    adamtest    adamtest    Adam        Test        123-12-1234 amill189@jh.edu  blah st
    adamtest2   adamtest2   adam2       test2       123-12-1234 adamtest2@jh.edu 123 fake st apt 2

REGISTRAR
    COURSE_ID  	USER_ID  
    605.784 adamtest
    605.784 adamtest2
    605.785 adamtest
    605.785 adamtest2
    605.786 adamtest
    605.787 adamtest
    605.788 adamtest

    
CONTENTS OF SUBMISSION

SrsAssignment9 - Java web application generated and built with Maven

SrsAssignment9/pom.xml - Maven POM file
SrsAssignment9/src/main/java/jhu/ep/amiller/* - Java source for assignment 9

SrsAssignment9/src/main/webapp/* - JSP and XHTML
SrsAssignment9/src/main/webapp/META-INF/persistence.xml - defines the JPA/Hibernate properties
SrsAssignment9/src/main/webapp/WEB-INF/jboss-web.xml
SrsAssignment9/src/main/webapp/WEB-INF/web.xml

SrsAssignment9/target/SrsAssignment9.war - packaged WAR file

screenshots.pdf - All requested screenshots of the configuration/Maven processes
README.txt - this file
SrsDb.* - the H2 database file(s)


BUILD/RUN/DEPLOY INSTRUCTIONS

Command to build with Maven: mvn package

Command to deploy to WildFly server: mvn clean wildfly:deploy

Web app URL on my machine: http://127.0.0.1:8080/SrsAssignment9/ or http://127.0.0.1:8080/SrsAssignment9/index.jsp


CONFIGURATION INFORMATION

To build the database schema, the following statements were executed:
CREATE TABLE STUDENT (User_ID varchar(64), Password varchar(16), First_Name varchar(64), Last_Name varchar(64), SSN varchar(16), Email varchar(64), Address varchar(64), PRIMARY KEY (User_ID));
CREATE TABLE COURSES (Course_ID varchar(16), Course_Title varchar(64));
CREATE TABLE REGISTRAR (Course_ID varchar(16), User_ID varchar(64), PRIMARY KEY(Course_ID, User_ID), FOREIGN KEY (Course_ID) references COURSES(Course_ID), FOREIGN KEY (User_ID) references STUDENT(User_ID));

INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.784', 'Enterprise Computing with Java');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.785', 'Web Services: Frameworks, Processes, Applications');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.786', 'Enterprise System Design and Implementation');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.787', 'Rich Internet Applications with Ajax');
INSERT INTO COURSES (Course_ID, Course_Title) VALUES ('605.788', 'Big Data Processing Using Hadoop');
INSERT INTO REGISTRAR (Course_ID, User_ID) VALUES ('605.784', 'adamtest'), ('605.785', 'adamtest'), ('605.786', 'adamtest'), ('605.787', 'adamtest'), ('605.788', 'adamtest');


To configure the datasource using jboss-cli, I used the following:
data-source add --jndi-name=java:jboss/datasources/H2_784_JNDI --name=H2_784_DS --connection-url=jdbc:h2:~/784/H2_784_DB;DB_CLOSE_DELAY=-1 --driver-name=h2
