package jhu.ep.amiller;

import java.io.Serializable;
import java.util.Date;

/**
 * Class used to pass the event from the view to the controller. Contains getters and setters for
 * the fields entered by the user.
 *
 * @author Adam
 * @date 2021-03-08
 */
public class ActionEvent implements Serializable {

  private static final long serialVersionUID = 6070589950990475422L;
  public String actionType;
  public Date datetime;
  private CoursesSupportBean coursesBean;
  private RegistrationSupportBean registrationBean;

  /**
   * Default constructor
   */
  public ActionEvent() {}

  public String getActionType() {
    return this.actionType;
  }

  public CoursesSupportBean getCoursesSupportBean() {
    return this.coursesBean;
  }

  public Date getDatetime() {
    return this.datetime;
  }

  public RegistrationSupportBean getRegistrationSupportBean() {
    return this.registrationBean;
  }

  public void setActionType(final String actionType) {
    this.actionType = actionType;
  }

  public void setCoursesBean(final CoursesSupportBean coursesBean) {
    this.coursesBean = coursesBean;

  }

  public void setDatetime(final Date datetime) {
    this.datetime = datetime;
  }

  public void setRegistrationBean(final RegistrationSupportBean regBean) {
    this.registrationBean = regBean;
  }

  @Override
  public String toString() {
    return this.actionType + " at " + this.datetime.toString();
  }
}
